import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';

const { REACT_APP_URL_API } = process.env;

class Telkom extends Component{

  constructor(props){
    super(props);
    this.state = {
      nama : props.nama,
      link : props.link,
      terminalid : props.terminalid,
      windowURL : props.windowURL,
      reqTelkom : {
        HEADER : header,
        billerid : props.billerid,
        customerid : "",
        userid : props.userid,
        partnerid : props.partnerid,
        publickey : props.publickey,
        email : props.email,
        extendinfo : "0",
        mobileno : props.mobileno,
        productid : "001",
        signature : ""
      }
    };

    if(this.state.reqTelkom.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }
  }

  handleChange = (event) => {
    let reqTelkomNew = {...this.state.reqTelkom}; //mengcopy variable dengan isi sama
    reqTelkomNew[event.target.name] = event.target.value;
    this.setState({
      reqTelkom: reqTelkomNew
    })
  }

  componentDidMount() {
    document.body.style.background = "white";
    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });
  }

  postdatatoAPI = () => {
    if(this.state.reqTelkom.customerid)
    {
      swal({
        text: "Mohon Tunggu",
        icon: require("./icon/loading3.gif"),
        buttons: false,
        closeOnClickOutside : false
      });
      // Create Signature
      var md5 = require('md5');
      let signature = md5(header+this.state.reqTelkom.userid+this.state.reqTelkom.partnerid+this.state.reqTelkom.publickey+this.state.reqTelkom.billerid+this.state.reqTelkom.customerid+this.state.reqTelkom.mobileno +this.state.reqTelkom.productid+password);
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      let reqTelkomNew = {...this.state.reqTelkom};
      reqTelkomNew['signature'] = ReSign;
      console.log("REQ TELKOM: ", reqTelkomNew)
      axios.post(REACT_APP_URL_API+'MobileInq',reqTelkomNew).then((res) => {
        console.log("RES TELKOM: ", res.data);
        if(res.data.rc === "00"){
          swal.close();
          const addDatas = JSON.parse(res.data.additionaldata);
          this.props.history.push('/detilpembayaran',{
            billerid : this.state.reqTelkom.billerid,
            productid : this.state.reqTelkom.productid,
            userid : this.state.reqTelkom.userid,
            partnerid : this.state.reqTelkom.partnerid,
            publickey : this.state.reqTelkom.publickey,
            customerid : res.data.customerid,
            customername : res.data.customername,
            totalamount : res.data.totalamount,
            additionaldata : res.data.additionaldata,
            addDatas : addDatas,
            mobileno : this.state.reqTelkom.mobileno,
            email : this.state.reqTelkom.email,
            extendinfo : res.data.extendinfo,
            trackingref : res.data.trackingref,
            username : this.state.terminalid,
            flag : "",
            img_detail : res.data.imgpth,
            namaproduct : res.data.title,
            n_product_detail : res.data.title,
            link : this.state.link,
          });    
        }else{
          swal("Oops..", res.data.rcdesc , "warning");
        }
      }, (err) => {
        swal("Error", err.message , "error");
      })
      .catch((error) => {
        // console.log(error.message);
        swal("Error", error.message , "error");
      })
    }else{
      swal("Oops..", "No Pelanggan harus diisi." , "warning");
    }
  }

  render(){
    return(
      <>
      <div className="navbarmenu">
        <Link to={this.state.windowURL} className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>  
      {/* <div className="jumbotron"></div> */}
      <div className="container-fluid main-menu">
        <div className="container2">
          <div className="h4">No. Pelanggan</div>
          <div><input className="input" type="number" id="customerid" min="0" name="customerid" placeholder="Nomor Pelanggan" onChange={this.handleChange} /></div>
        </div>
      </div>
      <div className="container-fluid button1" > 
        <hr/>
        <b.Button variant="success" name="btnproses" id="btnproses" onClick={this.postdatatoAPI} block>Continue</b.Button>
      </div>
      </>
    );
  };
}

export default Telkom;