import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import { Dropdown } from 'semantic-ui-react';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';
import NumberFormat from 'react-number-format';

const { REACT_APP_URL_API } = process.env;

class Voucher extends Component{

    constructor(props){
      super(props);
      this._isMounted = false;
      this.state = {
        nama : props.nama,
        windowURL : props.windowURL,
        reqVoucher : {
          HEADER : header,
          billerid : props.billerid,
          customerid : "",
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          email : props.email,
          extendinfo : "0",
          mobileno : props.mobileno,
          productid : "",
          signature : "",
          terminalid : props.terminalid,
          totalamount : ""
        },
        reqDataVoucher : {
          HEADER : header,
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          billerId : props.billerid,
          signature : ""
        },
        planets: [],
        dVoucher: [],
        filterVoucher: [],
        link: props.link,
      };

      if(this.state.reqVoucher.publickey === "" && this.state.windowURL === ""){
        swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
      }
    }

    componentDidMount() {
      document.body.style.background = "white";
      let windowURLNew = './solusipayweb/'+ this.state.windowURL;
      this.setState({
        windowURL: windowURLNew,
      });
      this._isMounted = true;
      // Generate Signature md5
      var md5 = require('md5');
      let signature = md5(header+this.state.reqDataVoucher.userid+this.state.reqDataVoucher.partnerid+this.state.reqDataVoucher.publickey+this.state.reqDataVoucher.billerId+password);
      // Buat Variable baru untuk mengUpdate Request Data MF
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      let reqDataVoucherNew = {...this.state.reqDataVoucher};
      reqDataVoucherNew['signature'] = ReSign;
      // Hit API POST 
      let initialPlanets = [];
      let detailVoucher = [];
      let no = 0;
      axios.post(REACT_APP_URL_API + 'ListProduct2',reqDataVoucherNew).then(response => {
        // console.log(response.data.data[0].detail);
        return response.data.data;
      }).then(data => {
        for (let i = 0; i < data.length; i++) {
          initialPlanets[i] = {key: data[i].groupid, value: data[i].groupid, image: require("../"+data[i].img), text: data[i].productdesc};
          for(let x=0; x < data[i].detail.length; x++){
            detailVoucher[no] = {billerid_detail: data[i].detail[x].billerid_detail, img_detail: data[i].detail[x].img_detail, keterangan_detail: data[i].detail[x].keterangan_detail, nominal_detail: data[i].detail[x].nominal_detail, productdesc_detail: data[i].detail[x].productdesc_detail, productid_detail: data[i].detail[x].productid_detail, groupid_detail: data[i].groupid}
            no = no + 1;
          }
        }

        // console.log(initialPlanets);
        // console.log(detailVoucher);
        if (this._isMounted) {
          this.setState({
            planets: initialPlanets,
            dVoucher: detailVoucher
          });
        }
      }).catch((error) => {
        console.log(error.message);
        swal({title: "ERROR - Server sedang maintenance.", text: "Kembali ke halaman utama.", icon: "error"}).then(function() {
          window.location = "./solusipayweb";
        });
      });
    }

    componentWillUnmount() {
      this._isMounted = false;
    }

    handleChange2 = (event,data) => {
      const result = this.state.dVoucher.filter(vocher => vocher.groupid_detail === data.value);
      
      this.setState({
        filterVoucher: result,
      })
      console.log(result,this.state.filterVoucher);

      // this.state.dVoucher.filter(vocher => vocher.groupid_detail > 11).map(filteredVocher => (
      //     console.log(filteredVocher)
      //   )
      // )
      
    }

    handleChange = (event,data) => {
      let reqVoucherNew = {...this.state.reqVoucher}; //mengcopy variable dengan isi sama
      if (data){
        // console.log(data.value);
        reqVoucherNew['productid'] = data.value;
      }else{
        // console.log(event.target.name,event.target.value);
        reqVoucherNew[event.target.name] = event.target.value;
      }
      this.setState({
        reqVoucher: reqVoucherNew
      })
    }
  
    postdatatoAPI = (nominaldetal,productdetail,productiddetail,imgdetail) => {
      let no_pel = this.state.reqVoucher.mobileno;

      let data = "{\"Flagaddtional\":\"1\",\"Adminvalue\":\"0\",\"Ls_Resadd\":[{\"label\":\"Nama Produk\",\"value\":\"" + productdetail + " \",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"ID Pel\",\"value\":\"" + no_pel + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Nama\",\"value\":\"" + this.state.reqVoucher.terminalid + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Tagihan\",\"value\":\"Rp " + nominaldetal + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Biaya Admin\",\"value\":\"Rp 0\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"}]}"

      let trckreff = Math.floor(Math.random() * 1000000).toString();
      let trck = 'SLS' + trckreff + this.state.reqVoucher.mobileno;
      const addDatas = JSON.parse(data);
      this.props.history.push('/detilpembayaran',{
        billerid : this.state.reqVoucher.billerid,
        productid : productiddetail,
        userid : this.state.reqVoucher.userid,
        partnerid : this.state.reqVoucher.partnerid,
        publickey : this.state.reqVoucher.publickey,
        customerid : no_pel,
        customername : this.state.reqVoucher.terminalid,
        totalamount : nominaldetal,
        additionaldata : data,
        addDatas : addDatas,
        mobileno : this.state.reqVoucher.mobileno,
        email : this.state.reqVoucher.email,
        extendinfo : this.state.reqVoucher.extendinfo,
        trackingref : trck,
        username : this.state.reqVoucher.terminalid,
        flag : "",
        img_detail : imgdetail,
        namaproduct : this.state.nama,
        n_product_detail : productdetail,
        link : this.state.link,
      });
    }
  
    render(){
      return(
        <>  
        <div className="navbarmenu">
          <Link to={this.state.windowURL} className="menu-barsmenu">
            <FaIcons.FaArrowLeft /> 
          </Link>
          <h2> {this.state.nama} </h2>
        </div>  
        {/* <div className="jumbotron"></div> */}
        <div className="container-fluid main-menu">
          <div className="container2">
            <div className="h4">Pilih Partner</div>
          </div>
          <Dropdown
            placeholder='Select Partner'
            fluid
            search
            selection
            options={this.state.planets}
            onChange={this.handleChange2}
          />
        </div>
        <div className="container-fluid" > 
          <br></br>
          <div>Denom Tersedia: </div>
          <b.ListGroup>
            {this.state.filterVoucher.map((item, index) => {
                return (
                  <b.ListGroup.Item className="lgItem" action key={item.productid_detail} onClick={this.postdatatoAPI.bind(this, item.nominal_detail, item.productdesc_detail, item.productid_detail,item.img_detail)} name="nominal" id="nominal" value={item.nominal_detail}>
                    <div className="container2">
                        <div className="h4">{item.productdesc_detail}</div>
                        <div className="price"><NumberFormat value={item.nominal_detail + ".00"} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
                    </div>
                  </b.ListGroup.Item>
                )
            })}
          </b.ListGroup>
        </div>
        </>
      );
    };
  }
  
  export default Voucher;