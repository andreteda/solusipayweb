import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';
import 'react-smarttab/dist/index.css';
import NumberFormat from 'react-number-format';
import './components/tab.css';
// import ReactPullToRefresh from 'react-pull-to-refresh';


const { REACT_APP_URL_API } = process.env;

class Aktivitas extends Component {

  constructor(props){
    super(props);
    this._isMounted = false;
    this.state = {
      nama : props.nama,
      link : props.link,
      prev_periode : "",
      dataAktif : [],
      dataPeriode : [],
      windowURL : props.windowURL,
      statusAktif: true,
      reqDataActivity : {
        HEADER : header,
        userid : props.userid,
        partnerid : props.partnerid,
        publickey : props.publickey,
        flag : "",
        signature : ""
      },
    };

    if(this.state.reqDataActivity.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }
  }

  componentDidMount(){
    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });

    swal({
      text: "Mohon Tunggu",
      icon: require("./icon/loading3.gif"),
      buttons: false,
      closeOnClickOutside : false
    });
    this._isMounted = true;
    // Generate Signature md5
    let fl = 0;
    var md5 = require('md5');
    let signature = md5(header+this.state.reqDataActivity.userid+this.state.reqDataActivity.partnerid+this.state.reqDataActivity.publickey+fl+password);
    // Buat Variable baru untuk mengUpdate Request Data MF
    let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
    let reqDataActivityNew = {...this.state.reqDataActivity};
    let dataAktifNew = [];
    let dataPeriodeNew = [];
    reqDataActivityNew['signature'] = ReSign;
    reqDataActivityNew['flag'] = fl;
    console.log("REQ AKTIFITAS: ", reqDataActivityNew);
    axios.post(REACT_APP_URL_API + 'Activity',reqDataActivityNew).then(response => {
      console.log("RES AKTIFITAS: ", response);
      return response;
    }).then(data => {
      if(data.status === 200){
        swal.close();
        const strojb = JSON.parse(data.request.response);
        console.log("DATA: ", strojb);
        let garis = "";
        for(let x=0; x < strojb.hdractivty.length; x++){
          let z = 0;
          dataPeriodeNew[x] = { periode : strojb.hdractivty[x].periode } // 
          for(let y = 0; y < strojb.hdractivty[x].ls_itemactivity.length; y++){
            if(z === 0 ){
              garis = "1";
            }else{
              let now = strojb.hdractivty[x].ls_itemactivity[y].datetrans.substring(0,strojb.hdractivty[x].ls_itemactivity[y].datetrans.length - 5);
              let previous = strojb.hdractivty[x].ls_itemactivity[y-1].datetrans.substring(0,strojb.hdractivty[x].ls_itemactivity[y-1].datetrans.length - 5);
              if(now === previous){
                garis = "0";
              }else{
                garis = "1";
              }
            }

            dataAktifNew[z] = { billerid : strojb.hdractivty[x].ls_itemactivity[y].biller , bookid : strojb.hdractivty[x].ls_itemactivity[y].bookid, customerid : strojb.hdractivty[x].ls_itemactivity[y].customerid, customername : strojb.hdractivty[x].ls_itemactivity[y].customername, datetrans : strojb.hdractivty[x].ls_itemactivity[y].datetrans, flag : strojb.hdractivty[x].ls_itemactivity[y].flag, imagpth : strojb.hdractivty[x].ls_itemactivity[y].imagpth, isexpire : strojb.hdractivty[x].ls_itemactivity[y].isexpire, no_transaction : strojb.hdractivty[x].ls_itemactivity[y].no_transaction, orderid: strojb.hdractivty[x].ls_itemactivity[y].orderid, pay_methode : strojb.hdractivty[x].ls_itemactivity[y].pay_methode, pay_methode_desc : strojb.hdractivty[x].ls_itemactivity[y].pay_methode_desc, productid : strojb.hdractivty[x].ls_itemactivity[y].productid, titel : strojb.hdractivty[x].ls_itemactivity[y].title, totalamount : strojb.hdractivty[x].ls_itemactivity[y].totalamount, periode : strojb.hdractivty[x].periode, garis : garis}
            z = z + 1;
          }
        }
        if (this._isMounted) {
          this.setState({
            dataPeriode: dataPeriodeNew,
            dataAktif : dataAktifNew,
          });
        }
        console.log("DATA PERIODE : ", dataPeriodeNew);
        console.log("DATA AKTIF : ", dataAktifNew);
      }else{
        swal("Oops..", data.rcdesc , "warning");
      }
    }).catch((error) => {
      // console.log(error.message);
      swal({title: "ERROR - Server sedang maintenance.", text: "Kembali ke halaman utama.", icon: "error"}).then(function() {
        window.location = "./solusipayweb";
      });
    });
  }

  postdatatoAPI = (title,orderid,bookingid,customername,totalamount,datetrans,no_pel,status,paymentmetode,imagpth,productid,billerid,token_struct) => {
    // console.log("LIST TOKEN STRUK: ", token_struct);
    this.props.history.push('/rincianaktivitas',{
      title : title,
      orderid : orderid,
      bookid : bookingid,
      customername : customername,
      totalamount : totalamount,
      datetrans : datetrans,
      no_pel : no_pel,
      status : status,
      paymentmetode : paymentmetode,
      version : "1",
      imagpth : imagpth,
      productid : productid,
      billerid : billerid,
      token_struct : token_struct
    });
  }

  refresh = (flag) => {
    if(flag === 0){
      this.setState({
        statusAktif: true,
      });
    }else if(flag === 1){
      this.setState({
        statusAktif: false,
      });
    }
    if (this._isMounted) {
      this.setState({
        dataPeriode: [],
        dataAktif : [],
      });
    }
    swal({
      text: "Mohon Tunggu",
      icon: require("./icon/loading3.gif"),
      buttons: false,
      closeOnClickOutside : false
    });
    let fl = flag;
    var md5 = require('md5');
    let signature = md5(header+this.state.reqDataActivity.userid+this.state.reqDataActivity.partnerid+this.state.reqDataActivity.publickey+fl+password);
    // Buat Variable baru untuk mengUpdate Request Data MF
    let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
    let reqDataActivityNew = {...this.state.reqDataActivity};
    let dataAktifNew = [];
    let dataPeriodeNew = [];
    reqDataActivityNew['signature'] = ReSign;
    reqDataActivityNew['flag'] = flag;
    console.log("REQ AKTIFITAS" + flag + ":" , reqDataActivityNew);
    axios.post(REACT_APP_URL_API + 'Activity',reqDataActivityNew).then(response => {
      console.log("RES AKTIFITAS" + flag + ": ", response);
      return response;
    }).then(data => {
      if(data.status === 200){
        swal.close();
        const strojb = JSON.parse(data.request.response);
        console.log("DATA: ", strojb);
        let garis = "";
        for(let x=0; x < strojb.hdractivty.length; x++){
          let z = 0;
          dataPeriodeNew[x] = { periode : strojb.hdractivty[x].periode } // 
          for(let y = 0; y < strojb.hdractivty[x].ls_itemactivity.length; y++){
            // console.log(y);
            if(z === 0 ){
              garis = "1";
            }else{
              let now = strojb.hdractivty[x].ls_itemactivity[y].datetrans.substring(0,strojb.hdractivty[x].ls_itemactivity[y].datetrans.length - 5);
              let previous = strojb.hdractivty[x].ls_itemactivity[y-1].datetrans.substring(0,strojb.hdractivty[x].ls_itemactivity[y-1].datetrans.length - 5);
              if(now === previous){
                garis = "0";
              }else{
                garis = "1";
              }
            }
            dataAktifNew[z] = { billerid : strojb.hdractivty[x].ls_itemactivity[y].biller , bookid : strojb.hdractivty[x].ls_itemactivity[y].bookid, customerid : strojb.hdractivty[x].ls_itemactivity[y].customerid, customername : strojb.hdractivty[x].ls_itemactivity[y].customername, datetrans : strojb.hdractivty[x].ls_itemactivity[y].datetrans, flag : strojb.hdractivty[x].ls_itemactivity[y].flag, imagpth : strojb.hdractivty[x].ls_itemactivity[y].imagpth, isexpire : strojb.hdractivty[x].ls_itemactivity[y].isexpire, no_transaction : strojb.hdractivty[x].ls_itemactivity[y].no_transaction, orderid: strojb.hdractivty[x].ls_itemactivity[y].orderid, pay_methode : strojb.hdractivty[x].ls_itemactivity[y].pay_methode, pay_methode_desc : strojb.hdractivty[x].ls_itemactivity[y].pay_methode_desc, productid : strojb.hdractivty[x].ls_itemactivity[y].productid, titel : strojb.hdractivty[x].ls_itemactivity[y].title, totalamount : strojb.hdractivty[x].ls_itemactivity[y].totalamount, token_struct: strojb.hdractivty[x].ls_itemactivity[y].token_struct , periode : strojb.hdractivty[x].periode, garis : garis}
            z = z + 1;
          }
        }
        if (this._isMounted) {
          this.setState({
            dataPeriode: dataPeriodeNew,
            dataAktif : dataAktifNew,
          });
        }
        console.log("DATA PERIODE : ", dataPeriodeNew);
        console.log("DATA AKTIF : ", dataAktifNew);
      }else{
        swal("Oops..", data.rcdesc , "warning");
      }
    }).catch((error) => {
      // console.log(error.message);
      swal({title: "ERROR - Server sedang maintenance.", text: "Kembali ke halaman utama.", icon: "error"}).then(function() {
        // window.location = "./solusipayweb";
      });
    });
  }

  render(){
    return (
      <>
      <div className="navbarmenu">
        <Link to={this.state.windowURL} className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>
      <br></br>
      <div className="container-fluid main-menu">
        <div className="containertab">
            <button type="button" className={this.state.statusAktif ? 'plntab' : 'plntab-non'} onClick={this.refresh.bind(this,0)}>  On Progress </button>
            <button type="button" className={this.state.statusAktif ? 'plntab-non' : 'plntab'} onClick={this.refresh.bind(this,1)}>  Completed </button>
        </div>
      </div>
      {/* <ReactPullToRefresh onRefresh={this.state.statusAktif ? this.refresh.bind(this,0) : this.refresh.bind(this,1)}> */}
      {
        this.state.statusAktif === true ? 
        (
          <b.ListGroup variant="flush">
            {this.state.dataAktif.map((item, index, arr) => {
              let ket = "";
              if(item.flag === "0" && item.isexpire === "0"){
                ket = "Menunggu Transfer";
              }else if(item.flag === "1" && item.isexpire === "0"){
                ket = "Menunggu Memilih Metode Pembayaran";
              }else if(item.flag === "2" && item.isexpire === "0"){
                ket = "Sedang Dalam Proses";
              }else if(item.isexpire === "1"){
                ket = "Kedaluarsa";
              }else if(item.isexpire === "2"){
                ket = "Dikembalikan";
              }else{
                ket = "Sukses Terbayar"
              }

              if(item.garis === "1"){
                return(
                  <div key={index}>
                  <div className="rowsBGActivity">{item.datetrans.substring(0,item.datetrans.length - 5)}</div> 
                    <b.ListGroup.Item className="aktivitasklik" action key={index} onClick={this.postdatatoAPI.bind(this, item.titel, item.orderid, item.bookid, item.customername,item.totalamount,item.datetrans, item.customerid, ket, item.pay_methode, item.imagpth, item.productid, item.billerid, "")} >
                      <div className="container2 row">
                        <div className="columnAktivitas">
                          <img src={require("../"+item.imagpth)} alt="" className="iconDetails3" />
                        </div>
                        <div className="columnAktivitas">
                          <div className="labelbold">{item.titel}</div>
                          <div className="label">{item.datetrans.substring(item.datetrans.length-5,item.datetrans.length)}</div>
                          <div className="label">{item.orderid}</div>
                          <div className="label">{ket}</div>
                        </div>
                        <div className="columnAktivitas">
                          <div className="iconDetails4 labelbold"><NumberFormat value={item.totalamount} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
                        </div>
                      </div>
                    </b.ListGroup.Item>
                  </div>
                )
              }else{
                return(
                  <div key={index}>
                  <b.ListGroup.Item action key={index} onClick={this.postdatatoAPI.bind(this, item.titel, item.orderid, item.bookid, item.customername,item.totalamount,item.datetrans, item.customerid, ket, item.pay_methode, item.imagpth, item.productid, item.billerid,"")} >
                    <div className="container2 row">
                      <div className="columnAktivitas">
                        <img src={require("../"+item.imagpth)} alt="" className="iconDetails3" />
                      </div>
                      <div className="columnAktivitas">
                        <div className="labelbold">{item.titel}</div>
                        <div className="label">{item.datetrans.substring(item.datetrans.length-5,item.datetrans.length)}</div>
                        <div className="label">{item.orderid}</div>
                        <div className="label">{ket}</div>
                      </div>
                      <div className="columnAktivitas">
                        <div className="iconDetails4 labelbold"><NumberFormat value={item.totalamount} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
                      </div>
                    </div>
                  </b.ListGroup.Item>
                  </div>
                )
              }
            })}
          </b.ListGroup>
        ):(
          <b.ListGroup variant="flush">
            {this.state.dataAktif.map((item, index) => {
              let ket = "";
              if(item.flag === "0" && item.isexpire === "0"){
                ket = "Menunggu Transfer";
              }else if(item.flag === "1" && item.isexpire === "0"){
                ket = "Menunggu Memilih Metode Pembayaran";
              }else if(item.flag === "2" && item.isexpire === "0"){
                ket = "Sedang Dalam Proses";
              }else if(item.isexpire === "1"){
                ket = "Kedaluarsa";
              }else if(item.isexpire === "2"){
                ket = "Dikembalikan";
              }else{
                ket = "Sukses Terbayar"
              }

              if(item.garis === "1"){
                return(
                  <div key={index}>
                  <div className="rowsBGActivity">{item.datetrans.substring(0,item.datetrans.length - 5)}</div> 
                    <b.ListGroup.Item action key={index} onClick={this.postdatatoAPI.bind(this, item.titel, item.orderid, item.bookid, item.customername,item.totalamount,item.datetrans, item.customerid, ket, item.pay_methode, item.imagpth, item.productid, item.billerid, item.token_struct)} >
                      <div className="container2 row">
                        <div className="columnAktivitas">
                          <img src={require("../"+item.imagpth)} alt="" className="iconDetails3" />
                        </div>
                        <div className="columnAktivitas">
                          <div className="labelbold">{item.titel}</div>
                          <div className="label">{item.datetrans.substring(item.datetrans.length-5,item.datetrans.length)}</div>
                          <div className="label">{item.orderid}</div>
                          <div className="label">{ket}</div>
                        </div>
                        <div className="columnAktivitas">
                          <div className="iconDetails4 labelbold"><NumberFormat value={item.totalamount} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
                        </div>
                      </div>
                    </b.ListGroup.Item>
                  </div>
                )
              }else{
                return(
                  <div key={index}>
                  <b.ListGroup.Item action key={index} onClick={this.postdatatoAPI.bind(this, item.titel, item.orderid, item.bookid, item.customername,item.totalamount,item.datetrans, item.customerid, ket, item.pay_methode, item.imagpth, item.productid, item.billerid, item.token_struct)} >
                    <div className="container2 row">
                      <div className="columnAktivitas">
                        <img src={require("../"+item.imagpth)} alt="" className="iconDetails3" />
                      </div>
                      <div className="columnAktivitas">
                        <div className="labelbold">{item.titel}</div>
                        <div className="label">{item.datetrans.substring(item.datetrans.length-5,item.datetrans.length)}</div>
                        <div className="label">{item.orderid}</div>
                        <div className="label">{ket}</div>
                      </div>
                      <div className="columnAktivitas">
                        <div className="iconDetails4 labelbold"><NumberFormat value={item.totalamount} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
                      </div>
                    </div>
                  </b.ListGroup.Item>
                  </div>
                )
              }
            })}
          </b.ListGroup>
        )  
      }
      {/* </ReactPullToRefresh> */}
      </>
    );
  }
}

export default Aktivitas;
