import React, {Component} from 'react';
import './App.css';
import swal from 'sweetalert';
import axios from 'axios';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
// import { Router, Switch, Route } from 'react-router-dom';
import MainMenu from './pages/mainMenu';
import Aktivitas from './pages/aktivitas';
import Home from './pages/home';
import RincianAktivitas from './pages/rincianaktivitas';
import Pln from './pages/pln';
import Bpjs from './pages/bpjs';
import Game from './pages/game.js';
import Insurance from './pages/insurance.js';
import Multifinance from './pages/multifinance.js';
import Pdam from './pages/pdam.js';
import Pgn from './pages/pgn.js';
import Postpaid from './pages/postpaid.js';
import Pulsa from './pages/pulsa.js';
import Kartukredit from './pages/kartukredit.js';
import Samsat from './pages/samsat.js';
import Telkom from './pages/telkom.js';
import Tv from './pages/tv.js';
import Voucher from './pages/voucher.js';
import { createBrowserHistory } from 'history';
import DetilPembayaran from './pages/detil_pembayaran';
import MetodePembayaran from './pages/metodePembyaran';
import HasilPembayaran from './pages/hasilPembayaran';
import Struk from './pages/struk';
import { header,password, rand1,rand2,rand3 } from './pages/data/mainData';

const { REACT_APP_URL_API } = process.env;
const history = createBrowserHistory();
const windowUrl = window.location.search;
const params = new URLSearchParams(windowUrl);

class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      publickey:"",
      email:"",
      mobileno:"",
      userid: "",
      data: "",
      redirect : false,
      reqTokenRegis : {
        trxdate : params.get('trxdate'),
        partnerid : params.get('partnerid'),
        token : params.get('token'),
        terminalid : params.get('terminalid'),
        signature  : params.get('signature')
      },
      reqGetBiller : {
        HEADER : header,
        userid : '',
        partnerid : params.get('partnerid'),
        publickey : '',
        signature: ''
      },
    };
  }

  componentDidMount() {
    swal({
      text: "Mohon Tunggu",
      icon: require("./pages/icon/loading3.gif"),
      buttons: false,
      closeOnClickOutside : false
    });
    if (this.state.reqTokenRegis.signature === null){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }else{
      let ReSign = rand3+this.state.reqTokenRegis.signature.substring(0,8)+rand1+this.state.reqTokenRegis.signature.substring(8,16)+rand2+this.state.reqTokenRegis.signature.substring(16,32);

      let reqTokenRegisNew = {...this.state.reqTokenRegis};
      reqTokenRegisNew['signature'] = ReSign;
      
      console.log("REQ TOKEN REGIS: ", reqTokenRegisNew);
      axios.post(REACT_APP_URL_API + 'TokenRegis',reqTokenRegisNew).then(response => {
        console.log("RES TOKEN REGIS: ", response.data);
        if (response.data.rc === "00"){
          this.setState({
            publickey: response.data.publickey,
            email: response.data.email,
            mobileno: response.data.mobilephone,
            userid: response.data.userid,
          });
          swal.close();
          // Hit API Get Biller
          var md5 = require('md5');
          let signature = md5(header + response.data.userid + params.get('partnerid') + response.data.publickey + password);

          let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
          let reqGetBillerNew = {...this.state.reqGetBiller};

          reqGetBillerNew['userid'] = response.data.userid;
          reqGetBillerNew['publickey'] = response.data.publickey;
          reqGetBillerNew['signature'] = ReSign;

          console.log("REQ GET BILLER: ",reqGetBillerNew);

          axios.post(REACT_APP_URL_API + 'GetBiller',reqGetBillerNew).then(response => {
            console.log("RES GET BILLER : ", response.data);
            return response.data;
          }).then(data => {
            if (data.rc === "00"){
              this.setState({
                data : data.data,
                redirect : true,
              })
            }else{
              swal("Error", data.rcdesc , "error");
            }
          }).catch((error) => {
            swal({title: "ERROR - Server sedang maintenance.", text: "Kembali ke halaman utama.", icon: "error"}).then(function() {
              window.location = "./solusipayweb";
            });
          });
        }else{
          swal("Error", response.data.rcdesc , "error");
        }
      }).catch((error) => {
        swal("Error", error.message , "error");
      })
        
      setTimeout(() => {
        swal("ERROR - Terjadi Kendala","Aplikasi otomatis kembali ke halaman utama.","error");
        window.location = './solusipayweb' + windowUrl;
      }, 3600000); //-000 seconds
    }
  }

  render(){
    return(
     <div>
      <Router history={history}>
        <Switch>
          {/* <Route path ='/solusipayweb' exact component={MainMenu}/> */}
          {
            this.state.redirect === true ? (
              <Route path ={'/solusipayweb'} 
                render={props=> 
                  <MainMenu {...props} 
                    data={this.state.data} 
                    billerid="" 
                    nama="PPOB" 
                    link="/solusipayweb" 
                  />
                }
              />
            ):(
              null
            )
          }
          
          <Route exact path ='/home' 
            render={props=> 
              <Home {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="" 
                nama="Home" 
                link="/home" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature}
                windowURL={windowUrl}
              />
            }
          />

          <Route exact path ='/aktivitas' 
            render={props=> 
              <Aktivitas {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="" 
                nama="Activity" 
                link="/aktivitas" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature}
                windowURL={windowUrl}
              />
            }
          />

          <Route exact path ='/rincianaktivitas' 
            render={props=> 
              <RincianAktivitas {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="" 
                nama="Activity Details" 
                link="/rincianaktivitas" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature}  
                windowURL={windowUrl}
              />
            }
          />

          <Route exact path ='/struk' 
            render={props=> 
              <Struk {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="" 
                nama="STRUK PEMBAYARAN" 
                link="/struk" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature}  
                windowURL={windowUrl}
              />
            }
          />

          <Route exact path='/pln_page' 
            render={props=> 
              <Pln {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="1" 
                nama="PLN" 
                link="/pln_page" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path='/bpjs_page' 
            render={props=> 
              <Bpjs {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="9" 
                nama="BPJS" 
                link="/bpjs_page" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path="/multifinance_page" 
            render={props=> 
              <Multifinance {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="10" 
                nama="MULTIFINANCE" 
                link="/multifinance_page" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path="/pdam_page" 
            render={props=> 
              <Pdam {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="2" 
                nama="PDAM" 
                link="/pdam_page" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            } 
          />

          <Route exact path="/postpaid_page" 
            render={props=> 
              <Postpaid {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="4"
                nama="POSTPAID" 
                link="/postpaid_page" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl} 
              />
            }  
          />

          <Route exact path="/insurance_page" 
            render={props=> 
              <Insurance {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="11" 
                nama="INSURANCE" 
                link="/insurance_page" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path="/tv_page" 
            render={props=> 
              <Tv {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="7" 
                nama="TV" 
                link="/tv_page" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path='/game' 
            render={props=> 
              <Game {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="13" 
                nama="VOUCHER GAME" 
                link="/game" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature}
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path='/voucher' 
            render={props=> 
              <Voucher {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="15" 
                nama="VOUCHER" 
                link="/voucher" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path='/pgn_page' 
            render={props=> 
              <Pgn {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="16" 
                nama="PGN" 
                link="/pgn_page" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path='/pulsa' 
            render={props=> 
              <Pulsa {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="12" 
                nama="PULSA" 
                link="/pulsa" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path='/telkom' 
            render={props=> 
              <Telkom {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="3" 
                nama="TELKOM" 
                link="/telkom" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
                />
              } 
            />

          <Route exact path='/esamsat' 
            render={props=> 
              <Samsat {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="17" 
                nama="E-SAMSAT"
                link="/esamsat" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path='/detilpembayaran' 
            render={props=> 
              <DetilPembayaran {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="" 
                nama="Payment Detail" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature}
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path='/metodepembayaran' 
            render={props=> 
              <MetodePembayaran {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="" 
                nama="Checkout" 
                link="/detilpembayaran" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
              />
            }  
          />

          <Route exact path='/hasilpembayaran' 
            render={props=> 
              <HasilPembayaran {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="" 
                nama="Payment Method" 
                link="/solusipayweb" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
                />
              }  
            />

          <Route exact path='/kartukredit_page' 
            render={props=> 
              <Kartukredit {...props} 
                publickey={this.state.publickey} 
                email={this.state.email} 
                mobileno={this.state.mobileno} 
                userid={this.state.userid} 
                billerid="14"
                nama="Credit Card" 
                link="/solusipayweb" 
                trxdate={this.state.reqTokenRegis.trxdate} 
                partnerid={this.state.reqTokenRegis.partnerid} 
                token={this.state.reqTokenRegis.token} 
                terminalid={this.state.reqTokenRegis.terminalid} 
                signature={this.state.reqTokenRegis.signature} 
                windowURL={windowUrl}
                />
              }  
            />
        </Switch>
      </Router>
    </div>
    );
  };
}

export default App;
