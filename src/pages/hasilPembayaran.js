import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';
import swal from 'sweetalert';
import NumberFormat from 'react-number-format';
// import dateFormat from 'dateformat';
import BCA from './payment/BCA.js';
import ATMBersama from './payment/ATMBersama.js';
import BRI from './payment/BRI.js';
import Mandiri from './payment/Mandiri.js';
import MandiriDirect from './payment/MandiriDirect.js';
import copy from "copy-to-clipboard"; 

class HasilPembayaran extends Component{
  constructor(props){
    super(props);
    this._isMounted = false;
    this.state = {
      publickey : props.publickey,
      windowURL : props.windowURL,
      nama : props.nama,
      dateNew : props.history.location.state.dateNew,
      paymentID : props.history.location.state.paymentID,
      pesan : false,
      reqHasil : {
        nova: props.history.location.state.nova,
        paymetode: props.history.location.state.paymetode,
        billerid: props.history.location.state.billerid,
        customername: props.history.location.state.customername,
        title: props.history.location.state.title,
        namaproduct: props.history.location.state.namaproduct,
        customerid: props.history.location.state.customerid,
        imgpth: props.history.location.state.imgpth,
        link: props.link,
        totalamount : props.history.location.state.totalamount,
        paymethdesc : props.history.location.state.paymethdesc,
        imgpath : props.history.location.state.imgpath,
      },
    }
  }

  componentDidMount(){
    this._isMounted = true;
    // console.log("PAYMENT ID: ",this.state.paymentID);

    if(this.state.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }
    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  toAktivitas(){
    window.location = "./aktivitas";
  }

  copyText = (teks) => {
    // navigator.clipboard.writeText(teks);
    copy(teks);
    this.setState({
        pesan : true,
      }
    )
    setTimeout(() => {
      this.setState({
          pesan : false,
        }
      )
    }, 1000); //-000 seconds
  }
  

  render(){
    return(
      <>  
      <div className="navbarmenu">
        <Link to={this.state.windowURL} className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>  
      <br></br>
      <div className="container-fluid" > 
        {
          this.state.pesan === true ? (
            <div className="pesan">Teks berhasil disalin!</div>
          ):(
            null
          )
        }
        <b.Card className="cardMetode">
          <div className="container2">
            <div>
              <img src={require("../"+ this.state.reqHasil.imgpth)} alt="" className="iconDetails2" />
            </div>
            <div className="h5">{this.state.reqHasil.namaproduct}</div>
            <div className="h5">{this.state.reqHasil.customerid} a/n {this.state.reqHasil.customername}</div>
            <div className="h5">Total: <NumberFormat value={this.state.reqHasil.totalamount} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
          </div>
        </b.Card>
        <div className="rows3">
          <div className="text1">Pembayaran melalui {this.state.reqHasil.paymethdesc}</div>
          <div className="logoVa"> <img src={require("../"+this.state.reqHasil.imgpath)} alt="" className="iconDetails5" /></div>
        </div>
        <div className="rows3">
          <div className="text2">Kirim pembayaran pada Nomor Virtual Account </div>
          <div className="textVA">{this.state.reqHasil.nova}</div>
          <button type="button" className="linkbuttonA" onClick={this.copyText.bind(this, this.state.reqHasil.nova)}>  SALIN </button>
        </div>
        <div className="rows3">
          <div className="text2">Total Pembayaran </div>
          <div className="nominalVA"> <NumberFormat value={this.state.reqHasil.totalamount} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /> </div>
          <button type="button" className="linkbuttonA" onClick={this.copyText.bind(this, this.state.reqHasil.totalamount)}>  SALIN </button>
        </div>
        <br></br>
        <b.Card className="cardMetode">
          <div className="container2">
            <div className="h6c">Selesaikan pembayaran sebelum: </div>
            <div className="h6b">{this.state.dateNew}</div>
          </div>
        </b.Card>
        <br></br>
        <div className="rows3">
          <div className="text2">Metode Pembayaran</div>
        </div>
        <br></br>
        {
          this.state.paymentID === "12" ? (
            <BCA />
          ):
          this.state.paymentID === "13" ? (
            <MandiriDirect />
          ):
          this.state.paymentID === "2" ? (
            <ATMBersama />
          ):
          this.state.paymentID === "1" ? (
            <Mandiri />
          ):
          this.state.paymentID === "17" ? (
            <BRI />
          ):
          ( null )
            
        }
      </div>
      <div className="container-fluid" >
        <hr/>
        <div><b.Button className="button2" variant="success" name="btnproses2" id="btnproses2" block><Link to={this.state.windowURL} className="link1"> Back to PPOB </Link></b.Button></div>
        <div><b.Button className="button3" variant="success" name="btnproses3" id="btnproses3" block><Link to='/aktivitas' className="link2"> Check Activity </Link></b.Button></div>
      </div>
      </>
    )
  }
}

export default HasilPembayaran;