import React, {} from 'react';
import * as b from 'react-bootstrap';

function Mandiri() {
  return (
    <>
    <b.Accordion defaultActiveKey="0">
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="0">
            ATM 
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="0">
            <b.Card.Body>
              1. Masukkan kartu ATM & PIN Mandiri Anda. <br></br>
              2. Pilih menu BAYAR / BELI > LAINNYA > LAINNYA > MULTIPAYMENT. <br></br>
              3. Masukan kode perusahaan i-pay 70017. <br></br>
              4. Masukkan nomor Mandiri Virtual Account (no virtual account). <br></br>
              5. Setelah muncul informasi pembayaran, pastikan jumlah tagihan sesuai dengan yang tertera pada detail transaksi Anda. <br></br>
              6. Masukkan angka 1 lalu pilih YA untuk menyelesaikan transaksi.
              7. Setelah pembayaran sukses, simpan struk pembayaran Anda sebagai bukti yang sah.
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="1">
            Mobile
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="1">
            <b.Card.Body>
              1. Login dengan masukkan password Anda pada aplikasi Mandiri Mobile Anda <br></br>
              2. Pilih menu BAYAR > LAINNYA > i-pay. <br></br>
              3. Masukkan nomor Mandiri Virtual Account (no virtual account). <br></br>
              4. Tekan tombol LANJUT. <br></br>
              5. Setelah muncul informasi pembayaran pastikan jumlah tagihan sesuai dengan yang tertera pada detail transaksi Anda. <br></br>
              6. Masukkan PIN Mandiri Mobile Anda lalu tekan OK. <br></br>
              7. Setelah pembayaran sukses simpan struk pembayaran Anda sebagai bukti yang sah.
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="2">
            Internet
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="2">
            <b.Card.Body>
              1. Login dengan masukkan User ID dan password Anda ke Layar. <br></br>
              2. Pilih menu BAYAR > MULTIPAYMENT. <br></br>
              3. Pilih Dari Rekening : Rekening Anda lalu pilih Penyedia Jasa : i-pay. <br></br>
              4. Masukkan no.ref/kode mitra :. <br></br>
              5. Setelah muncul informasi pembayaran pastikan jumlah tagihan sesuai dengan yang tertera pada detail transaksi Anda. <br></br>
              6. Tekan LANJUTKAN dan ikuti transaksi selanjutnya. <br></br>
              7. Setelah pembayaran sukses simpan struk pembayaran Anda sebagai bukti yang sah.
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
      </b.Accordion>
    </>
  )
}

export default Mandiri;
