import React, { Component } from 'react'
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './Navbar.css';

class Navbar extends Component{

  handleClick = () => { 
    window.myFunction();  console.log('this is:', this);  
  }

  render(){
    return (
      <>
        <div className="navbarmenu">
          <div onClick={this.handleClick} className="menu-barsmenu">
            <FaIcons.FaArrowLeft /> 
          </div>
          <div className="ppob"><h2>PPOB</h2></div>
          {/* <button onClick={Print.postMessage('Hello World being called from Javascript code')}>Try it</button> */}
          <div className="activity"><h2><Link to="/aktivitas">Activity</Link></h2></div>
        </div>
      </>
    )
  }
}

export default Navbar;
