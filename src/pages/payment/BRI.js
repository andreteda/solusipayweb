import React, {} from 'react';
import * as b from 'react-bootstrap';

function BRI() {
  return (
    <>
    <b.Accordion defaultActiveKey="0">
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="0">
            ATM BRI
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="0">
            <b.Card.Body>
              1. Pilih menu Transaksi Lain, kemudian pilih menu Pembayaran. <br></br>
              2. Pilih menu Lainnya, lalu pilih menu BRIVA. <br></br>
              3. Masukkan nomor rekening dengan nomor Virtual Account dan pilih Benar. <br></br>
              4. Ketika muncul konfirmasi pembayaran, silahkan pilih Ya. <br></br>
              5. Transaksi telah selesai dan silahkan ambil bukti pembayaran Anda.
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="1">
            Mobile BRI
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="1">
            <b.Card.Body>
              1. Pilih menu Pembayaran. <br></br>
              2. Pilih menu BRIVA. <br></br>
              3. Masukkan nomor rekening dengan nomor Virtual Account. <br></br>
              4. Masukkan PIN Mobile Banking dan klik Kirim. <br></br>
              5. STransaksi selesai.
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="2">
            Internet Banking
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="2">
            <b.Card.Body>
              1. Pilih menu Pembayaran. <br></br>
              2. Pilih menu BRIVA. <br></br>
              3. Masukkan nomor rekening dengan nomor Virtual Account kemudian klik Kirim. <br></br>
              4. Masukkan password serta mToken internet banking. <br></br>
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
      </b.Accordion>
    </>
  )
}

export default BRI;
