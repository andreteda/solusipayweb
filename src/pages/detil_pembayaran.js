import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';
import NumberFormat from 'react-number-format';
import axios from 'axios';
import swal from 'sweetalert';

const { REACT_APP_URL_API } = process.env;

class DetilPembayaran extends Component{
  constructor(props){
    super(props);
    this._isMounted = false;
    this.state = {
      nama : props.nama,
      publickey : props.publickey,
      img_detail : props.history.location.state.img_detail,
      namaproduct : props.history.location.state.namaproduct,
      n_product_detail : props.history.location.state.n_product_detail,
      link : props.history.location.state.link,
      listMetodeByr : [],
      windowURL : props.windowURL,
      addDatas : props.history.location.state.addDatas,
      reqMobilePay : {
        HEADER : header,
        billerid : props.history.location.state.billerid,
        productid : props.history.location.state.productid,
        userid : props.history.location.state.userid,
        partnerid : props.history.location.state.partnerid,
        publickey : props.history.location.state.publickey,
        customerid : props.history.location.state.customerid,
        customername : props.history.location.state.customername,
        totalamount : props.history.location.state.totalamount,
        additionaldata : props.history.location.state.additionaldata,
        mobileno : props.history.location.state.mobileno,
        email : props.history.location.state.email,
        extendinfo : props.history.location.state.extendinfo,
        trackingref : props.history.location.state.trackingref,
        signature : "",
        username : props.history.location.state.username,
        flag : props.history.location.state.flag,
      },
    }
  }

  componentDidMount(){
    document.body.style.background = "white";
    this._isMounted = true;
    // console.log("ADD DATAS: ",this.state.reqMobilePay.additionaldata.Ls_Resadd);
    // console.log(this.state.img_detail);

    if(this.state.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }

    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });
  }

  postdatatoAPI = () => {
    swal({
      text: "Mohon Tunggu",
      icon: require("./icon/loading3.gif"),
      buttons: false,
      closeOnClickOutside : false
    });
    // Generate Signature md5
    var md5 = require('md5');
    let signature = md5(header+this.state.reqMobilePay.userid+this.state.reqMobilePay.partnerid+this.state.reqMobilePay.publickey+this.state.reqMobilePay.billerid+this.state.reqMobilePay.productid+this.state.reqMobilePay.additionaldata+this.state.reqMobilePay.mobileno+this.state.reqMobilePay.customerid+password);
    // Buat Variable baru untuk mengUpdate Request Data MF
    let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
    let reqMobilePayNew = {...this.state.reqMobilePay};
    let listMetodeByrNew = [];
    let bookingid = "";
    let orderid = "";
    reqMobilePayNew['signature'] = ReSign;
    console.log("REQ MOBILE PAY:",reqMobilePayNew);
    axios.post(REACT_APP_URL_API + 'MobilePay',reqMobilePayNew).then(response => {
      console.log("RES MOBILE PAY:",response.data);
      return response.data;
    }).then(data => {
      if(data.rc === "00"){
        swal.close();
        const strojb = JSON.parse(data.strobj);
        // console.log("DATA: ", strojb);
        // console.log("ISI DATA: ", strojb.ls_mp[0]);
        // console.log("DATA LENGTH: ", strojb.ls_mp.length);
        bookingid = data.bookingid;
        orderid = data.orderid;
        for(let x=0; x < strojb.ls_mp.length; x++){
          listMetodeByrNew[x] = { item_1 : strojb.ls_mp[x].item_1, item_2 : strojb.ls_mp[x].item_2, item_3 : strojb.ls_mp[x].item_3 }
        }
        if (this._isMounted) {
          this.setState({
            listMetodeByr: listMetodeByrNew,
          });
        }
        // console.log("LIST METODE BAYAR: ", this.state.listMetodeByr);
        this.props.history.push('/metodepembayaran',{
          billerid : this.state.reqMobilePay.billerid,
          productid : this.state.reqMobilePay.productid,
          userid : this.state.reqMobilePay.userid,
          partnerid : this.state.reqMobilePay.partnerid,
          publickey : this.state.reqMobilePay.publickey,
          customerid : this.state.reqMobilePay.customerid,
          customername : this.state.reqMobilePay.customername,
          totalamount : this.state.reqMobilePay.totalamount,
          additionaldata : this.state.reqMobilePay.additionaldata,
          mobileno : this.state.reqMobilePay.mobileno,
          email : this.state.reqMobilePay.email,
          extendinfo : this.state.reqMobilePay.extendinfo,
          trackingref : this.state.reqMobilePay.trackingref,
          username : this.state.reqMobilePay.username,
          flag : "",
          img_detail : this.state.img_detail,
          namaproduct : this.state.namaproduct,
          n_product_detail : this.state.n_product_detail,
          listMetodeBayar : this.state.listMetodeByr,
          bookingid : bookingid,
          orderid : orderid
        });
      }else{
        swal("Oops..", data.rcdesc , "warning");
      }
    }).catch((error) => {
      console.log(error.message);
      swal({title: error.message, text: "Kembali ke halaman sebelumnya.", icon: "error"}).then(function() {
        // window.location = "./solusipayweb";
      });
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }
  
  render(){
    return(
      <>  
      <div className="navbarmenu">
        <Link to={this.state.windowURL} className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>  
      <div className="paymentBG">
        <img src={require("../"+ this.state.img_detail)} alt="" className="iconDetailspay" />
      </div>
        <b.Card className="card1">
            <div className="cart">CART</div>
            <div className="dashed"></div>
            {this.state.addDatas.Ls_Resadd
            .filter(addData => addData.type === "text")
            .map((item, index) => {
              // if(item.type === "text" ){
                return (
                  <div className="rows3" key={index}>
                    <div className="title">{item.label}</div>
                    <div className="sub_title">{item.value}</div>
                  </div>
                )
              // }
            })}
            <div className="rows3">
              <hr className="garishijau"/>
              <div className="titleTotal">Total : </div>
              <div className="sub_titleTotal"><NumberFormat value={this.state.reqMobilePay.totalamount}  displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
            </div>
        </b.Card>
        <div className="container-fluid button1" >
          <hr/>
          <b.Button variant="success" name="btnproses" id="btnproses" onClick={this.postdatatoAPI} block>Choose Payment</b.Button>
        </div>
      </>
    )
  }
}

export default DetilPembayaran;