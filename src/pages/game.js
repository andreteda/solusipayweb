import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import { Dropdown } from 'semantic-ui-react';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/tab.css';
import NumberFormat from 'react-number-format';

const { REACT_APP_URL_API } = process.env;

class VoucherGame extends Component{

    constructor(props){
      super(props);
      this._isMounted = false;
      this.state = {
        loading: true,
        nama : props.nama,
        windowURL : props.windowURL,
        reqVoucherGame : {
          HEADER : header,
          billerid : props.billerid,
          customerid : "",
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          email : props.email,
          extendinfo : "",
          mobileno : props.mobileno,
          productid : "",
          signature : "",
          terminalid : props.terminalid,
          totalamount : ""
        },
        reqDataVoucherGame : {
          HEADER : header,
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          billerId : props.billerid,
          signature : ""
        },
        planets: [],
        dVoucherGame: [],
        filterVoucherGame: [],
        fl_ML: false,
        link: props.link,
      };

      if(this.state.reqVoucherGame.publickey === "" && this.state.windowURL === ""){
        swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
      }
    }

    componentDidMount() {
      document.body.style.background = "white";
      let windowURLNew = './solusipayweb/'+ this.state.windowURL;
      this.setState({
        windowURL: windowURLNew,
      });
      this._isMounted = true;
      // Generate Signature md5
      var md5 = require('md5');
      let signature = md5(header+this.state.reqDataVoucherGame.userid+this.state.reqDataVoucherGame.partnerid+this.state.reqDataVoucherGame.publickey+this.state.reqDataVoucherGame.billerId+password);
      // Buat Variable baru untuk mengUpdate Request Data MF
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      let reqDataVoucherGameNew = {...this.state.reqDataVoucherGame};
      reqDataVoucherGameNew['signature'] = ReSign;
      // Hit API POST 
      let initialPlanets = [];
      let detailVoucherGame = [];
      let no = 0;
      axios.post(REACT_APP_URL_API + 'ListProduct2',reqDataVoucherGameNew).then(response => {
        console.log(response.data.data);
        return response.data.data;
      }).then(data => {
        for (let i = 0; i < data.length; i++) {
          initialPlanets[i] = {key: data[i].groupid, value: data[i].groupid, image: require("../"+data[i].img), text: data[i].productdesc};
          for(let x=0; x < data[i].detail.length; x++){
            detailVoucherGame[no] = {billerid_detail: data[i].detail[x].billerid_detail, img_detail: data[i].detail[x].img_detail, keterangan_detail: data[i].detail[x].keterangan_detail, nominal_detail: data[i].detail[x].nominal_detail, productdesc_detail: data[i].detail[x].productdesc_detail, productid_detail: data[i].detail[x].productid_detail, groupid_detail: data[i].groupid}
            no = no + 1;
          }
        }

        if (this._isMounted) {
          this.setState({
            planets: initialPlanets,
            dVoucherGame: detailVoucherGame
          });
        }
      }).catch((error) => {
        console.log(error.message);
        swal({title: "ERROR - Server sedang maintenance.", text: "Kembali ke halaman utama.", icon: "error"}).then(function() {
          window.location = "./solusipayweb";
        });
      });
    }

    componentWillUnmount() {
      this._isMounted = false;
    }

    handleChange2 = (event,data) => {
      const result = this.state.dVoucherGame.filter(vocher => vocher.groupid_detail === data.value);
      console.log(result);
      this.setState({
        filterVoucherGame: result,
      })

      if (data.value === "1"){
        this.setState({
          fl_ML: true
        });
      }else{
        this.setState({
          fl_ML: false
        });
      }
    }

    handleChange = (event) => {
      // Khussus untuk Voucher Mobile Legends
      let reqVoucherGameNew = {...this.state.reqVoucherGame};
      reqVoucherGameNew[event.target.name] = event.target.value;
      this.setState({
        reqVoucherGame: reqVoucherGameNew
      })
    }
  
    postdatatoAPI = (nominaldetal,productdetail,productiddetail,imgdetail) => {
      if((this.state.reqVoucherGame.customerid && this.state.fl_ML === true) || this.state.fl_ML === false)
      {
        let no_pel = "";
        if(this.state.fl_ML === true){
          no_pel = this.state.reqVoucherGame.customerid;
        }else{
          no_pel = this.state.reqVoucherGame.mobileno;
        }

        let data = "{\"Flagaddtional\":\"1\",\"Adminvalue\":\"0\",\"Ls_Resadd\":[{\"label\":\"Nama Produk\",\"value\":\"" + productdetail + " \",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"ID Pel\",\"value\":\"" + no_pel + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Nama\",\"value\":\"" + this.state.reqVoucherGame.terminalid + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Tagihan\",\"value\":\"Rp " + nominaldetal + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Biaya Admin\",\"value\":\"Rp 0\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"}]}"

        let trckreff = Math.floor(Math.random() * 1000000).toString();
        let trck = 'SLS' + trckreff + this.state.reqVoucherGame.mobileno;
        const addDatas = JSON.parse(data);
        this.props.history.push('/detilpembayaran',{
          billerid : this.state.reqVoucherGame.billerid,
          productid : productiddetail,
          userid : this.state.reqVoucherGame.userid,
          partnerid : this.state.reqVoucherGame.partnerid,
          publickey : this.state.reqVoucherGame.publickey,
          customerid : no_pel,
          customername : this.state.reqVoucherGame.terminalid,
          totalamount : nominaldetal,
          additionaldata : data,
          addDatas : addDatas,
          mobileno : this.state.reqVoucherGame.mobileno,
          email : this.state.reqVoucherGame.email,
          extendinfo : this.state.reqVoucherGame.extendinfo,
          trackingref : trck,
          username : this.state.reqVoucherGame.terminalid,
          flag : "",
          img_detail : imgdetail,
          namaproduct : this.state.nama,
          n_product_detail : productdetail,
          link : this.state.link,
        });
      }else{
        if(!this.state.reqVoucherGame.customerid){
          swal("Oops..", "No Pelanggan harus diisi." , "warning");
        }
      }
    }
  
    render(){
      return(
        <>  
        <div className="navbarmenu">
          <Link to={this.state.windowURL} className="menu-barsmenu">
            <FaIcons.FaArrowLeft /> 
          </Link>
          <h2> {this.state.nama} </h2>
        </div>  
        {/* <div className="jumbotron"></div> */}
        <div className="container-fluid main-menu">
          <div className="container2">
            <div className="h4">Pilih Partner</div>
          </div>
          <Dropdown
            placeholder='Select Partner'
            fluid
            search
            selection
            options={this.state.planets}
            onChange={this.handleChange2}
          />
        </div>
        <div className="container-fluid" > 
          {
            this.state.fl_ML === true ?
            (
              <>
              <br></br>
              <div className="container2">
                <div className="h4">No. Pelanggan</div>
                <div><input className="input" type="number" id="customerid" min="0" name="customerid" placeholder="Nomor Pelanggan" onChange={this.handleChange} /></div>
              </div>
              <br></br>
              <b.Card>
                <div className="container2">
                  <div>
                    <img src={require('./icon/fyi.png')} alt="" className="iconDetails" />
                  </div>
                  <div className="h6d">Untuk mengetahui User ID Anda, Silahkan Klik menu profile dibagian kiri atas pada menu utama game. Dan user ID akan terlihat dibagian bawah Nama Karakter Game Anda. Silakan masukkan User ID Anda untuk menyelesaikan transaksi. Contoh : 12345678</div>
                </div>
              </b.Card>
              </>
            )
            :
            (
              null
            )
          }
          <br></br>
          <div>Denom Tersedia: </div>
          {/* <b.ListGroup> */}
            {this.state.filterVoucherGame.map((item, index) => {
                return (
                  <button type="button" key={item.productid_detail} className="nominalGame" onClick={this.postdatatoAPI.bind(this, item.nominal_detail, item.productdesc_detail, item.productid_detail,item.img_detail)} name="nominal" id="nominal" value={item.nominal_detail}>{item.productdesc_detail} <br></br> <NumberFormat className="green" value={item.nominal_detail + ".00"} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></button>
                  // <b.ListGroup.Item className="lgItem" action key={item.productid_detail} onClick={this.postdatatoAPI.bind(this, item.nominal_detail, item.productdesc_detail, item.productid_detail,item.img_detail)} name="nominal" id="nominal" value={item.nominal_detail}>
                  //   <div className="container2">
                  //       <div className="h4">{item.productdesc_detail}</div>
                  //       <div className="price"><NumberFormat value={item.nominal_detail + ".00"} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
                  //   </div>
                  // </b.ListGroup.Item>
                )
            })}
          {/* </b.ListGroup> */}
        </div>
        </>
      );
    };
  }
  
  export default VoucherGame;