import React, {Component} from 'react';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';
import { header,password,rand1,rand2,rand3 } from './data/mainData';

const { REACT_APP_URL_API } = process.env;

class pulsa extends Component{

  constructor(props){
    super(props);
    this.state = {
      nama : props.nama,
      link : props.link,
      terminalid : props.terminalid,
      windowURL : props.windowURL,
      flPulsa : true,
      reqPulsa : {
        HEADER : header,
        billerid : props.billerid,
        customerid : "",
        userid : props.userid,
        partnerid : props.partnerid,
        publickey : props.publickey,
        email : props.email,
        extendinfo : "0",
        mobileno : props.mobileno,
        productid : "",
        signature : ""
      },
      reqDataPulsa : {
        HEADER : header,
        userid : props.userid,
        partnerid : props.partnerid,
        publickey : props.publickey,
        billerId : props.billerid,
        prefix : "",
        signature : ""
      },
      planets: [],
      produkPulsa: [],
    };

    if(this.state.reqPulsa.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }
  }

  componentDidMount(){
    document.body.style.background = "white";
    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });
  }

  handleChange = (event) => {
    let reqDataPulsaNew = {...this.state.reqDataPulsa}; //mengcopy variable dengan isi sama
    let reqPulsaNew = {...this.state.reqPulsa};
    reqPulsaNew[event.target.name] = event.target.value;
    this.setState({
      reqDataPulsa: reqDataPulsaNew,
      reqPulsa : reqPulsaNew
    });
    if((event.target.value.length === 4 || event.target.value.length === 12) && this.state.flPulsa === true){
      this._isMounted = true;
      // Generate Signature md5
      var md5 = require('md5');
      let detailProdukPulsa = [];
      let signature = md5(header+this.state.reqDataPulsa.userid+this.state.reqDataPulsa.partnerid+this.state.reqDataPulsa.publickey+this.state.reqDataPulsa.billerId+event.target.value+password);
      // Buat Variable baru untuk mengUpdate Request Data MF
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      reqDataPulsaNew['prefix'] = event.target.value;
      reqDataPulsaNew['signature'] = ReSign;
      console.log("REQ PAKET PULSA: ",reqDataPulsaNew);
      axios.post(REACT_APP_URL_API+ 'ListProductPulsa',reqDataPulsaNew).then((res) => {
        console.log("RES PAKET PULSA: ",res.data);
        for (let i = 0; i < res.data.detail.data.length; i++) {
          detailProdukPulsa[i] = {billerid : res.data.detail.data[i].billerid, img : res.data.detail.data[i].img, ispromo : res.data.detail.data[i].ispromo, keterangan : res.data.detail.data[i].keterangan, nominal : res.data.detail.data[i].nominal, nominal_spromo : res.data.detail.data[i].nominal_spromo, productdesc : res.data.detail.data[i].productdesc, productid : res.data.detail.data[i].productid }
        }

        if (this._isMounted) {
          this.setState({
            produkPulsa: detailProdukPulsa,
            flPulsa : false,
          });
        }
      }, (err) => {
        swal("Error", err.message , "error");
      })
      .catch((error) => {
        // console.log(error.message);
        swal("Error", error.message , "error");
      })
    }else if(event.target.value.length < 4){
      if (this._isMounted) {
        this.setState({
          produkPulsa: [],
          flPulsa : true,
        });
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  postdatatoAPI = (productid, nominal, imgdetail, productdetail) => {

    let data = "{\"Flagaddtional\":\"1\",\"Adminvalue\":\"0\",\"Ls_Resadd\":[{\"label\":\"Nama Produk\",\"value\":\"" + productdetail + " \",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"ID Pel\",\"value\":\"" + this.state.reqPulsa.customerid + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Nama\",\"value\":\"" + this.state.terminalid + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Tagihan\",\"value\":\"Rp " + nominal + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Biaya Admin\",\"value\":\"Rp 0\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"}]}"

    let trckreff = Math.floor(Math.random() * 1000000).toString();
    let trck = 'SLS' + trckreff + this.state.reqPulsa.mobileno;
    const addDatas = JSON.parse(data);
    this.props.history.push('/detilpembayaran',{
      billerid : this.state.reqPulsa.billerid,
      productid : productid,
      userid : this.state.reqPulsa.userid,
      partnerid : this.state.reqPulsa.partnerid,
      publickey : this.state.reqPulsa.publickey,
      customerid : this.state.reqPulsa.customerid,
      customername : this.state.terminalid,
      totalamount : nominal,
      additionaldata : data,
      addDatas : addDatas,
      mobileno : this.state.reqPulsa.mobileno,
      email : this.state.reqPulsa.email,
      extendinfo : this.state.reqPulsa.extendinfo,
      trackingref : trck,
      username : this.state.terminalid,
      flag : "",
      img_detail : imgdetail,
      namaproduct : this.state.nama,
      n_product_detail : productdetail,
      link : this.state.link,
    });

    // if(this.state.reqPulsa.customerid)
    // {
    //   swal({
    //     text: "Mohon Tunggu",
    //     icon: require("./icon/loading3.gif"),
    //     buttons: false,
    //     closeOnClickOutside : false
    //   });
    //   // Create Signature
    //   var md5 = require('md5');
    //   let signature = md5(header+userid+partnerid+this.state.reqPulsa.publickey+this.state.reqPulsa.billerid+this.state.reqPulsa.customerid+this.state.reqPulsa.mobileno +productid+password);
    //   let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
    //   let reqPulsaNew = {...this.state.reqPulsa};
    //   reqPulsaNew['productid'] = productid;
    //   reqPulsaNew['signature'] = ReSign;
    //   console.log("REQ PULSA: ",reqPulsaNew);
    //   // hit API
    //   axios.post(REACT_APP_URL_API+'MobileInq',reqPulsaNew).then((res) => {
    //     console.log("RES PULSA: ",res.data);
    //     if(res.data === "00"){
    //       swal.close();
    //       this.props.history.push('/detilpembayaran',{
    //         billerid : this.state.reqPulsa.billerid,
    //         productid : this.state.reqPulsa.productid,
    //         userid : this.state.reqPulsa.userid,
    //         partnerid : this.state.reqPulsa.partnerid,
    //         publickey : this.state.reqPulsa.publickey,
    //         customerid : res.data.customerid,
    //         customername : res.data.customername,
    //         totalamount : res.data.totalamount,
    //         additionaldata : res.data.additionaldata,
    //         mobileno : this.state.reqPulsa.mobileno,
    //         email : this.state.reqPulsa.email,
    //         extendinfo : res.data.extendinfo,
    //         trackingref : res.data.trackingref,
    //         username : terminalid,
    //         flag : "",
    //         img_detail : res.data.imgpth,
    //         namaproduct : res.data.title,
    //         n_product_detail : res.data.title,
    //         link : this.state.link,
    //       });    
    //     }else{
    //       swal("Oops..", res.data.rcdesc , "warning");
    //     }
    //   }, (err) => {
    //     swal("Error", err.message , "error");
    //   })
    //   .catch((error) => {
    //     // console.log(error.message);
    //     swal("Error", error.message , "error");
    //   })

    // }else{
    //   swal("Oops..", "No Pelanggan harus diisi." , "warning");
    // }
  }

  render(){
    return(
      <>
      <div className="navbarmenu">
        <Link to={this.state.windowURL} className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>  
      {/* <div className="jumbotron"></div> */}
      <div className="container-fluid main-menu">
        <div className="container2">
          <div className="h4">No. Pelanggan</div>
          <div><input className="input" type="number" id="customerid" min="0" name="customerid" placeholder="Nomor Pelanggan" onChange={this.handleChange} /></div>
        </div>
      </div>
      <div className="container-fluid">
      {/* <b.ListGroup variant="flush"> */}
        {this.state.produkPulsa.map((item, index) => {
            return (
              <button type="button" key={item.productid}  className="nominal" onClick={this.postdatatoAPI.bind(this, item.productid, item.nominal, item.img, item.poductdesc)} name="nominal" id="nominal" value={item.nominal}>{item.productdesc}</button>
              // <b.ListGroup.Item action key={item.productid} onClick={this.postdatatoAPI.bind(this, item.productid, item.nominal, item.img, item.poductdesc)} name="nominal" id="nominal" value={item.nominal}>
              //   <div className="container2">
              //     <div>
              //       <div className="h4">{item.productdesc}</div>
              //       <div className="price"><NumberFormat value={item.nominal + ".00"} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
              //     </div>
              //   </div>
              // </b.ListGroup.Item>
            )
        })}
      {/* </b.ListGroup> */}
      </div>
      </>
    );
  };
}

export default pulsa;