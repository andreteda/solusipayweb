import React, {Component} from 'react';
import axios from 'axios';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import swal from 'sweetalert';
import { Dropdown } from 'semantic-ui-react';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';
import { header,password,rand1,rand2,rand3 } from './data/mainData';

const { REACT_APP_URL_API } = process.env;

class kartukredit extends Component{

  constructor(props){
    super(props);
    this.state = {
      nama : props.nama,
      link : props.link,
      terminalid : props.terminalid,
      fl_Inq : "0",
      windowURL : props.windowURL,
      flkartukredit : true,
      reqkartukredit : {
        HEADER : header,
        billerid : props.billerid,
        customerid : "",
        userid : props.userid,
        partnerid : props.partnerid,
        publickey : props.publickey,
        email : props.email,
        extendinfo : "0",
        mobileno : props.mobileno,
        productid : "",
        signature : "",
        amount : "",
        productname : "",
      },
      reqDatakartukredit : {
        HEADER : header,
        userid : props.userid,
        partnerid : props.partnerid,
        publickey : props.publickey,
        billerId : props.billerid,
        prefix : "",
        signature : ""
      },
      planets: [],
    };

    if(this.state.reqkartukredit.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }
  }

  componentDidMount(){
    document.body.style.background = "white";
    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });
    this._isMounted = true;
    var md5 = require('md5');
    let signature = md5(header+this.state.reqDatakartukredit.userid+this.state.reqDatakartukredit.partnerid+this.state.reqDatakartukredit.publickey+this.state.reqDatakartukredit.billerId+password);
    // Buat Variable baru untuk mengUpdate Request Data MF
    let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
    let reqDatakartukreditNew = {...this.state.reqDatakartukredit};
    reqDatakartukreditNew['signature'] = ReSign;
    console.log("REQ DATA KARTU KREDIT: ", reqDatakartukreditNew);
    // Hit API POST 
    let initialPlanets = [];
      axios.post(REACT_APP_URL_API + 'ListProduct',reqDatakartukreditNew).then(response => {
        console.log("RES DATA KARTU KREDIT: ", response.data.data);
        return response.data.data;
    }).then(data => {
      for (let i = 0; i < data.length; i++) {
        // if(data[i].img){
        //   initialPlanets[i] = {key: data[i].productid, value: data[i].productid, image: require("../"+data[i].img), text: data[i].productdesc};
        //   // console.log("1");
        // }else{
          initialPlanets[i] = {key: data[i].productid, value: data[i].productid, image: '', text: data[i].productdesc, groupid: data[i].groupid};
        //   // console.log("2");
        // }
        
      }
      // console.log(initialPlanets);
      if (this._isMounted) {
        this.setState({
          planets: initialPlanets,
        });
      }
    }).catch((error) => {
      // console.log(error.message);
      swal({title: "ERROR - Server sedang maintenance.", text: "Kembali ke halaman utama.", icon: "error"}).then(function() {
        window.location = "./solusipayweb";
      });
    });
  }

  handleChange = (event,data) => {
    let reqkartukreditNew = {...this.state.reqkartukredit};
    let namaproduct = [];
    let arr_groupid = [];
    let groupid = "0";
    if (data){
      reqkartukreditNew['productid'] = data.value;
      namaproduct = data.options.filter(dataOpt => dataOpt.key === data.value);
      arr_groupid = data.options.filter(dataOpt => dataOpt.key === data.value);
      groupid = arr_groupid[0].groupid;
      reqkartukreditNew['productname'] = namaproduct[0].text;
      this.setState({
        fl_Inq : groupid,
      });
    }else{
      reqkartukreditNew[event.target.name] = event.target.value;
    }
    this.setState({
      reqkartukredit : reqkartukreditNew
    });
    this._isMounted = true;
    
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  postdatatoAPI = () => {
    // console.log(this.state.reqkartukredit);
    if(this.state.fl_Inq === "1"){
      if(this.state.reqkartukredit.customerid !== "" && this.state.reqkartukredit.productid !== "" && (this.state.reqkartukredit.amount !== "0" && this.state.reqkartukredit.amount !== ""))
      {
        let imgdetail = "assets/imgs/home/CC.png"

        let data = "{\"Flagaddtional\":\"1\",\"Adminvalue\":\"0\",\"Ls_Resadd\":[{\"label\":\"Nama Produk\",\"value\":\"" + this.state.reqkartukredit.productname + " \",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"ID Pel\",\"value\":\"" + this.state.reqkartukredit.customerid + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Nama\",\"value\":\"" + this.state.terminalid + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Tagihan\",\"value\":\"Rp " + this.state.reqkartukredit.amount + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Biaya Admin\",\"value\":\"Rp 0\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"}]}"

        let trckreff = Math.floor(Math.random() * 1000000).toString();
        let trck = 'SLS' + trckreff + this.state.reqkartukredit.mobileno;
        const addDatas = JSON.parse(data);
        this.props.history.push('/detilpembayaran',{
          billerid : this.state.reqkartukredit.billerid,
          productid : this.state.reqkartukredit.productid,
          userid : this.state.reqkartukredit.userid,
          partnerid : this.state.reqkartukredit.partnerid,
          publickey : this.state.reqkartukredit.publickey,
          customerid : this.state.reqkartukredit.customerid,
          customername : this.state.terminalid,
          totalamount : this.state.reqkartukredit.amount,
          additionaldata : data,
          addDatas : addDatas,
          mobileno : this.state.reqkartukredit.mobileno,
          email : this.state.reqkartukredit.email,
          extendinfo : this.state.reqkartukredit.extendinfo,
          trackingref : trck,
          username : this.state.terminalid,
          flag : "",
          img_detail : imgdetail,
          namaproduct : this.state.nama,
          n_product_detail : this.state.reqkartukredit.productname,
          link : this.state.link,
        });
      }else{
        console.log("test");
        if(this.state.reqkartukredit.productid === ""){
          swal("Oops..", "Pilih salah satu product." , "warning");
        }else if(this.state.reqkartukredit.customerid === ""){
          swal("Oops..", "No Pelanggan harus diisi." , "warning");
        }else if (this.state.reqkartukredit.amount === "" || this.state.reqkartukredit.amount === "0"){
          swal("Oops..", "Nominal harus diisi." , "warning");
        }
      }
    }
  }

  render(){
    return(
      <>
      <div className="navbarmenu">
        <Link to={this.state.windowURL} className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>  
      {/* <div className="jumbotron"></div> */}
      <div className="container-fluid main-menu">
        <div className="container2">
            <div className="h4">Pilih Partner</div>
          </div>
          <Dropdown
            placeholder='Select Partner'
            fluid
            search
            selection
            options={this.state.planets}
            onChange={this.handleChange}
          />
        <div className="container2">
          <div className="h4">No. Pelanggan</div>
          <div><input className="input" type="number" id="customerid" min="0" name="customerid" placeholder="Nomor Pelanggan" onChange={this.handleChange}/></div>
        </div>
        <div className="container2">
          <div className="h4">Nominal</div>
          <div><input className="input" type="number" id="amount" min="0" name="amount" placeholder="Nominal" onChange={this.handleChange}/></div>
        </div>
      </div>
      <div className="container-fluid button1" > 
          <hr/>
          <b.Button variant="success" name="btnproses" id="btnproses" onClick={this.postdatatoAPI} block>Continue</b.Button>
        </div>
      </>
    );
  };
}

export default kartukredit;