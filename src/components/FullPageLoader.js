import React, {Component} from 'react';
import LoaderGif from '../assets/imgs/Loading_icon.gif';
import './Navbar.css';

class FullPageLoader extends Component{
  state = {}

  render(){
    return (
      <>
        <div className="loader-container">
          <div className="loader">
            <img src={LoaderGif} alt="" />
          </div>
        </div>
      </>
    );
  };
}

export default FullPageLoader;