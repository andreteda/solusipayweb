import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import {password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';
// import { Tabs, TabNav, TabNavItem, TabContent, TabPanel } from 'react-smarttab';
import 'react-smarttab/dist/index.css';
// import NumberFormat from 'react-number-format';

const { REACT_APP_URL_API } = process.env;

class Struk extends Component {

  constructor(props){
    super(props);
    this._isMounted = false;
    this.state = {
      nama : props.nama,
      publickey : props.publickey,
      partnerid : props.partnerid,
      userid : props.userid,
      windowURL : props.windowURL,
      token_struct : props.history.location.state.token_struct,
      arrStruk : {
        tanggallunas : props.history.location.state.tanggallunas,
        noInvoice : props.history.location.state.noInvoice,
        // noResi : props.history.location.state.noResi,
        namaProduk : props.history.location.state.namaProduk,
        // idPel : props.history.location.state.idPel,
        // nama : props.history.location.state.nama,
        // ref : props.history.location.state.ref,
        // tagihan : props.history.location.state.tagihan,
        // totalBayar : props.history.location.state.totalBayar,
        additionaldata : props.history.location.state.additionaldata,
        imgpth : props.history.location.state.imgpth,
      },
     
    };

    if(this.state.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }
  }

  componentDidMount(){
    // console.log("TOKEN STRUK: ", this.state.token_struct);
    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });
    // console.log("STRUK: ",this.state.arrStruk);
    // console.log("LABEL: ",this.state.arrStruk.additionaldata[1].label);
  }

  downloadPDF = () => {
    // let token = "3fa27a718646450d7552260a884f3fae8c21d2cb";
    let token = this.state.token_struct;
    var md5 = require('md5');
    let signature = md5(token+this.state.userid+this.state.partnerid+this.state.publickey+password);
    let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);

    axios.get(REACT_APP_URL_API + 'downloadinv/?' 
      + 'token=' + token + '&'
      + 'userid=' + this.state.userid + '&'
      + 'partnerid=' + this.state.partnerid + '&'
      + 'publickey=' + this.state.publickey + '&'
      + 'signature=' + ReSign, 
      {responseType : 'blob'})
    .then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data], {type: 'application/pdf'}));
      // console.log("URL DATA: ", url.substring(5,url.length));
      // console.log("URL DATA: ", response.config.url);
      // const link = document.createElement('a');
      // link.target = '_blank';
      // link.href = url;
      // link.setAttribute('download', 'struk.pdf');
      window.open(url);
      // swal("Info", "url: " + url , "info");
      // window.open(link.click(), "test", "height=200,width=200");
      // document.body.appendChild(link);
      window.myDownload("URL:"+response.config.url);  
      console.log('this is:', this);  
      
    })
    .catch(function (error) {
      swal({title: error.message, text: "Kembali ke halaman sebelumnya.", icon: "error"}).then(function() {
        // window.location = "./solusipayweb";
      });
    });
  }

  render(){
    return (
      <>
      <div className="navbarmenu">
        <Link to={this.state.windowURL} className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>
      <div className="container-fluid" >
        <div>Invoice Tagihan {this.state.arrStruk.additionaldata[0].value}</div> 
        <div className="container2">
          <div>
            <img src={require("../"+ this.state.arrStruk.imgpth)} alt="" className="iconDetailsSTruk" />
          </div>
          <div className="h5"> Tanggal Lunas : {this.state.arrStruk.tanggallunas}</div>
          <div className="h5"> Nomor Invoice : {this.state.arrStruk.noInvoice}</div>
          <div className="h5"><button type="button" className="linkbutton" onClick={this.downloadPDF} >Unduh Invoice</button></div>
        </div>
        <hr></hr>
        {this.state.arrStruk.additionaldata
        .filter(addData => (addData.type === "text" || addData.type === "textb"))
        .map((item, index) => {
          return (
            <b.Row className="struk" key={index}>
              <b.Col xs="5">{item.label}</b.Col>
              <b.Col xs="2">:</b.Col>
              <b.Col>{item.value}</b.Col>
            </b.Row>
          )
        })}
        {this.state.arrStruk.additionaldata
        .filter(addData => addData.type === "pesan")
        .map((item, index) => {
          // if(item.type === "pesan" ){
            return (
              <b.Row className="text-center struk" key={index}>
                <b.Col>{item.label}</b.Col>
              </b.Row>
            )
          // }
        })}
      </div>
      </>
    );
  }
}

export default Struk;
