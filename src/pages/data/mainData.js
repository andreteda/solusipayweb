let date = new Date();
var randomstring = require("randomstring");

export const header = JSON.stringify({
  version: "1.2.6", //1.2.6
  UUID: "15fd72137585c7f4",
  serialD: "c7990c0c9905",
  trxdate: date,
  merek: "samsung"
});

// export const trxdate = "20191104165410";
// export const partnerid = "DMY";
// export const token = "XYTD5678HA";
// export const terminalid = "IDS";
// export const signature = "a99c1f3cb56678b0e157bb089a034a2149ed8aa6373f2";
// export const userid = "DMY12234";
export const password = "5olusipay";
export const respassword = "s0lusipay";

export const rand3 = randomstring.generate({
  length: 5,
  charset: 'abcdefghijklmnopqrstuvwxyz0123456789'
});
export const  rand1 = randomstring.generate({
  length: 4,
  charset: 'abcdefghijklmnopqrstuvwxyz0123456789'
});
export const  rand2 = randomstring.generate({
  length: 4,
  charset: 'abcdefghijklmnopqrstuvwxyz0123456789'
});

/*
REQ : 
{
  "HEADER":"{
              \"version\":\"1.2.6\",
              \"UUID\":\"15fd72137585c7f4\",
              \"serialD\":\"c7990c0c9905\",
              \"trxdate\":\"2020-11-03T02:57:31.643Z\",
              \"merek\":\"samsung\"
            }",
  "billerid":"13",
  "productid":"001",
  "userid":"DMY12234",
  "partnerid":"DMY",
  "publickey":"14657b55c0b55e2476fb9078734eea7797312ef6",
  "customerid":"123456789",
  "customername":"IDS",
  "totalamount":"10300",
  "additionaldata":"{
                      \"Flagaddtional\":\"1\",
                      \"Adminvalue\":\"0\",
                      \"Ls_Resadd\":[
                                      {
                                        \"label\":\"Nama Produk\",
                                        \"value\":\"MOBILE LEGEND 36 DIAMOND \",
                                        \"required\":\"mandatory\",
                                        \"type\":\"text\",
                                        \"jumspasi\":\"0\"
                                      },
                                      {
                                        \"label\":\"ID Pel\",
                                        \"value\":\"123456789\",
                                        \"required\":\"mandatory\",
                                        \"type\":\"text\",
                                        \"jumspasi\":\"0\"
                                      },
                                      {
                                        \"label\":\"Nama\",
                                        \"value\":\"IDS\",
                                        \"required\":\"mandatory\",
                                        \"type\":\"text\",
                                        \"jumspasi\":\"0\"
                                      },
                                      {
                                        \"label\":\"Tagihan\",
                                        \"value\":\"Rp 10300\",
                                        \"required\":\"mandatory\",
                                        \"type\":\"text\",
                                        \"jumspasi\":\"0\"
                                      },
                                      {
                                        \"label\":\"Biaya Admin\",
                                        \"value\":\"Rp 0\",
                                        \"required\":\"mandatory\",
                                        \"type\":\"text\",
                                        \"jumspasi\":\"0\"
                                      }
                                    ]
                    }",
  "mobileno":"087886352096",
  "email":"labib@ids.id",
  "extendinfo":"",
  "trackingref":"SLS755652087886352096",
  "signature":"n357i7738ea03u0qa7043814fwohk3111db03379ac048",
  "username":"IDS",
  "flag":""
}
*/
