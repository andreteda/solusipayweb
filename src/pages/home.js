import React from "react";
import { render } from "react-dom";
import { Dropdown } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showing_modal: false
    };
  }

  componentDidMount() {
    // I want to do something like the next line here, but 'focus' is not available on the component.
    //this.MyDropdown.focus();
  }

  render() {
    const styleInner = {
      width: "50%",
      marginLeft: "3em",
      marginTop: "3em"
    };

    const options = [{ text: "hi", value: "1" }, { text: "ho", value: "2" }];

    const dd = (
      <Dropdown
        ref={dd => (this.MyDropdown = dd)}
        placeholder="Please type something, dude"
        fluid
        search
        selection
        options={options}
        onSelect={() => document.activeElement.blur()}
      />
    );

    return (
      <div style={styleInner}>
        <h2>
          Type a search term (without manually focusing the Dropdown below
          first!):
        </h2>
        {dd}
        <h4>It won't work like this.</h4>
      </div>
    );
  }
}

render(<Home />, document.getElementById("root"));

export default Home;