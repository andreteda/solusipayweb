import React, {} from 'react';
import * as b from 'react-bootstrap';

function MandiriDirect() {
  return (
    <>
      <b.Accordion defaultActiveKey="0">
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="0">
            ATM
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="0">
            <b.Card.Body>
              1. Masukkan kartu ATM. <br></br>
              2. Kemudian pilih BAHASA INDONESIA. <br></br>
              3. Ketik nomor PIN kartu ATM, kemudian tekan ENTER. <br></br>
              4. Pada menu utama, Pilih menu BAYAR/BELI. <br></br>
              5. Pilih menu MULTIPAYMENT. <br></br>
              6. Ketik kode perusahaan, yaitu ”23020” tekan BENAR. Atau,. <br></br>
              7.Klik DAFTAR KODE untuk mencari kode SOLUSIPAY, yaitu 23020. <br></br>
              8.	Isi NO VIRTUAL, kemudian tekan BENAR. <br></br>
              9.	Muncul konfirmasi data customer. Pilih Nomor 1 sesuai tagihan yang akan dibayar, kemudian tekan YA. <br></br>
              10.	Muncul konfirmasi pembayaran. Tekan YA untuk melakukan pembayaran. <br></br>
              11.	Bukti Pembayaran dalam bentuk STRUK agar disimpan sebagai bukti pembayaran yang sah dari Bank Mandiri. <br></br>
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="1">
            Mobile
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="1">
            <b.Card.Body>
              1. Masuk ke aplikasi Mandiri Online, kemudian masukkan User ID dan Password yang benar. <br></br>
              2. Pada menu utama, pilih menu PEMBAYARAN. <br></br>
              3. Pilih menu MULTIPAYMENT. <br></br>
              4. Pada kolom PENYEDIA JASA, pilih SOLUSIPAY. <br></br>
              5. Isi NO VIRTUAL, (untuk kolom DESKRIPSI merupakan optional) kemudian tekan LANJUT. <br></br>
              6. a.  Pada Mandiri Online versi Web, OTP (One Time Password) akan dikirimkan ke nomor handphone yang didaftarkan untuk fasilitas Mandiri Online. OTP diinput ke token untuk mendapat Challenge Code. Challenge Code diinput ke mandol. Klik lanjut. <br></br>
              b.  Sedangkan pada Mandiri Online versi Apps, setelah muncul Konfirmasi Transaksi, tekan KIRIM, kemudian masukkan MPIN (6 digit). <br></br>
              7. Setelah berhasil, akan muncul pembayaran dalam bentuk STRUK yang bisa didonlot dan disimpan sebagai bukti pembayaran yang sah dari Bank Mandiri.
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="2">
            Cabang Mandiri
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="2">
            <b.Card.Body>
              1. Ambil aplikasi setoran/transfer/kliring/inkaso dan isilan tanggal sesuai dengan tanggal pada saat melakukan transaksi. <br></br>
              2. Beri tanda pada kolom :. <br></br>
              a.	“Setoran ke Rekening Sendiri” jika sumber dana tunai. <br></br>
              b.	“Transfer” jika sumber dana debet rekening. <br></br>
              3.	Beri tanda pada kolom “Penduduk” jika penerima dan pengirim adalah penduduk Indonesia. <br></br>
              4.	Isi nama pengirim sesuai dengan nama penyetor. <br></br>
              5.	Isi nama penerima dengan SOLUSIPAY (23020). <br></br>
              6.	Isilah nama Bank dengan BANK MANDIRI. <br></br>
              7.	Pada Nomor rekening, isilah dengan NO VIRTUAL. <br></br>
              8.	Beri tanda pada sumber dana transaksi (tunai/debet rekening).
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
      </b.Accordion>
    </>
  )
}

export default MandiriDirect;
