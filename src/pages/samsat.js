import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import { Dropdown } from 'semantic-ui-react';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';

const { REACT_APP_URL_API } = process.env;

class Samsat extends Component{

    constructor(props){
      super(props);
      this._isMounted = false;
      this.state = {
        nama : props.nama,
        link : props.link,
        terminalid : props.terminalid,
        windowURL : props.windowURL,
        selected : "",
        reqSamsat : {
          HEADER : header,
          billerid : props.billerid,
          customerid : "",
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          email : props.email,
          extendinfo : "1",
          mobileno : props.mobileno,
          productid : "",
          signature : ""
        },
        reqDataSamsat : {
          HEADER : header,
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          billerId : props.billerid,
          signature : ""
        },
        planets: [],
      };

      if(this.state.reqSamsat.publickey === "" && this.state.windowURL === ""){
        swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
      }
    }

    componentDidMount() {
      this._isMounted = true;
      // Generate Signature md5
      var md5 = require('md5');
      let signature = md5(header+this.state.reqDataSamsat.userid+this.state.reqDataSamsat.partnerid+this.state.reqDataSamsat.publickey+this.state.reqDataSamsat.billerId+password);
      // Buat Variable baru untuk mengUpdate Request Data MF
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      let reqDataSamsatNew = {...this.state.reqDataSamsat};
      reqDataSamsatNew['signature'] = ReSign;
      // Hit API POST 
      let initialPlanets = [];
      axios.post(REACT_APP_URL_API + 'ListProduct',reqDataSamsatNew).then(response => {
        console.log(response.data.data);
        return response.data.data;
      }).then(data => {
        for (let i = 0; i < data.length; i++) {
          initialPlanets[i] = {key: data[i].productid, value: data[i].productid, image: require("../"+data[i].img), text: data[i].productdesc};
          if(this.state.selected === ""){
            this.setState({selected : data[i].productid})
          }
        }
        console.log("SELECTED: ",this.state.selected);
        if (this._isMounted) {
          this.setState({
            planets: initialPlanets,
          });
        }
      }).catch((error) => {
        // console.log(error.message);
        swal({title: "ERROR - Server sedang maintenance.", text: "Kembali ke halaman utama.", icon: "error"}).then(function() {
          window.location = "./solusipayweb";
        });
      });
    }

    componentWillUnmount() {
      this._isMounted = false;
    }

    handleChange = (event,data) => {
      let reqSamsatNew = {...this.state.reqSamsat}; //mengcopy variable dengan isi sama
      if (data){
        // console.log(data.value);
        reqSamsatNew['productid'] = data.value;
      }else{
        // console.log(event.target.name,event.target.value);
        reqSamsatNew[event.target.name] = event.target.value;
      }
      this.setState({
        reqSamsat: reqSamsatNew
      })
    }
  
    postdatatoAPI = () => {
      if(this.state.reqSamsat.customerid && this.state.reqSamsat.productid)
      {
        swal({
          text: "Mohon Tunggu",
          icon: require("./icon/loading3.gif"),
          buttons: false,
          closeOnClickOutside : false
        });
        // Create Signature
        var md5 = require('md5');
        let signature = md5(header+this.state.reqSamsat.userid+this.state.reqSamsat.partnerid+this.state.reqSamsat.publickey+this.state.reqSamsat.billerid+this.state.reqSamsat.customerid+this.state.reqSamsat.mobileno +this.state.reqSamsat.productid+password);
        let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
        let reqSamsatNew = {...this.state.reqSamsat};
        reqSamsatNew['signature'] = ReSign;
        console.log("REQ SAMSAT: ", reqSamsatNew);
        // hit API
        axios.post(REACT_APP_URL_API+'MobileInq',reqSamsatNew).then((res) => {
          console.log("RES SAMSAT: ",res.data);
          if(res.data.rc === "00"){
            swal.close();
            const addDatas = JSON.parse(res.data.additionaldata);
            this.props.history.push('/detilpembayaran',{
              billerid : this.state.reqSamsat.billerid,
              productid : this.state.reqSamsat.productid,
              userid : this.state.reqSamsat.userid,
              partnerid : this.state.reqSamsat.partnerid,
              publickey : this.state.reqSamsat.publickey,
              customerid : res.data.customerid,
              customername : res.data.customername,
              totalamount : res.data.totalamount,
              additionaldata : res.data.additionaldata,
              addDatas : addDatas,
              mobileno : this.state.reqSamsat.mobileno,
              email : this.state.reqSamsat.email,
              extendinfo : res.data.extendinfo,
              trackingref : res.data.trackingref,
              username : this.state.terminalid,
              flag : "",
              img_detail : res.data.imgpth,
              namaproduct : res.data.title,
              n_product_detail : res.data.title,
              link : this.state.link,
            });  
          }else{
            swal("Oops..", res.data.rcdesc , "warning");
          }
        }, (err) => {
          swal("Error", err.message , "error");
        })
        .catch((error) => {
          // console.log(error.message);
          swal("Error", error.message , "error");
        })
      }else{
        if(!this.state.reqSamsat.productid){
          swal("Oops..", "Pilih salah satu product." , "warning");
        }else if(!this.state.reqSamsat.customerid){
          swal("Oops..", "No Pelanggan harus diisi." , "warning");
        }
      }
    }
  
    render(){
      return(
        <>
        <div className="navbarmenu">
          <Link to="/solusipayweb" className="menu-barsmenu">
            <FaIcons.FaArrowLeft /> 
          </Link>
          <h2> {this.state.nama} </h2>
        </div>    
        <div className="container-fluid main-menu">
          <div className="container2">
            <div className="h4">Pilih Partner</div>
          </div>
          <Dropdown
            placeholder='Select Partner'
            fluid
            search
            selection
            options={this.state.planets}
            defaultValue={this.state.selected}
            onChange={this.handleChange}
          />
          <div className="container2">
            <div>
              <img src={require('./icon/esamsat.png')} alt="Samsat" className="iconDetails" />
            </div>
            <div>
              <div className="h4">No. Pelanggan</div>
              <div><input className="input" type="number" id="customerid" min="0" name="customerid" placeholder="Nomor Pelanggan" onChange={this.handleChange} /></div>
            </div>
          </div>
        </div>
        <div className="container-fluid button1" > 
          <hr/>
          <b.Button variant="primary" name="btnproses" id="btnproses" onClick={this.postdatatoAPI} block>Proses</b.Button>
        </div>
        </>
      );
    };
  }
  
  export default Samsat;