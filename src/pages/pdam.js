import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import { Dropdown } from 'semantic-ui-react';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import './components/mainMenu.css';

const { REACT_APP_URL_API } = process.env;

class Pdam extends Component{

    constructor(props){
      super(props);
      this._isMounted = false;
      this.state = {
        nama : props.nama,
        link : props.link,
        terminalid : props.terminalid,
        windowURL : props.windowURL,
        reqPDAM : {
          HEADER : header,
          billerid : props.billerid,
          customerid : "",
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          email : props.email,
          extendinfo : "0",
          mobileno : props.mobileno,
          productid : "",
          signature : ""
        },
        reqDataPDAM : {
          HEADER : header,
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          billerId : props.billerid,
          signature : ""
        },
        planets: [],
      };

      if(this.state.reqPDAM.publickey === "" && this.state.windowURL === ""){
        swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
      }
    }

    componentDidMount() {
      document.body.style.background = "white";
      let windowURLNew = './solusipayweb/'+ this.state.windowURL;
      this.setState({
        windowURL: windowURLNew,
      });
      this._isMounted = true;
      // Generate Signature md5
      var md5 = require('md5');
      let signature = md5(header+this.state.reqDataPDAM.userid+this.state.reqDataPDAM.partnerid+this.state.reqDataPDAM.publickey+this.state.reqDataPDAM.billerId+password);
      // Buat Variable baru untuk mengUpdate Request Data MF
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      let reqDataPDAMNew = {...this.state.reqDataPDAM};
      reqDataPDAMNew['signature'] = ReSign;
      console.log("REQ DATA PDAM: ", reqDataPDAMNew);
      // Hit API POST 
      let initialPlanets = [];
        axios.post(REACT_APP_URL_API + 'ListProduct',reqDataPDAMNew).then(response => {
          console.log("RES DATA PDAM: ", response.data.data);
          return response.data.data;
      }).then(data => {
        for (let i = 0; i < data.length; i++) {
          if(data[i].img){
            initialPlanets[i] = {key: data[i].productid, value: data[i].productid, image: require("../"+data[i].img), text: data[i].productdesc};
          }else{
            initialPlanets[i] = {key: data[i].productid, value: data[i].productid, image: '', text: data[i].productdesc};
          }
          
        }
        console.log(initialPlanets);
        if (this._isMounted) {
          this.setState({
            planets: initialPlanets,
          });
        }
      }).catch((error) => {
        // console.log(error.message);
        swal({title: "ERROR - Server sedang maintenance.", text: "Kembali ke halaman utama.", icon: "error"}).then(function() {
          window.location = "./solusipayweb";
        });
      });
    }

    componentWillUnmount() {
      this._isMounted = false;
    }

    handleChange = (event,data) => {
      let reqPDAMNew = {...this.state.reqPDAM}; //mengcopy variable dengan isi sama
      if (data){
        // console.log(data.value);
        reqPDAMNew['productid'] = data.value;
      }else{
        // console.log(event.target.name,event.target.value);
        reqPDAMNew[event.target.name] = event.target.value;
      }
      this.setState({
        reqPDAM: reqPDAMNew
      })
    }
  
    postdatatoAPI = () => {
      if(this.state.reqPDAM.customerid && this.state.reqPDAM.productid)
      {
        swal({
          text: "Mohon Tunggu",
          icon: require("./icon/loading3.gif"),
          buttons: false,
          closeOnClickOutside : false
        });
        // Create Signature
        var md5 = require('md5');
        let signature = md5(header+this.state.reqPDAM.userid+this.state.reqPDAM.partnerid+this.state.reqPDAM.publickey+this.state.reqPDAM.billerid+this.state.reqPDAM.customerid+this.state.reqPDAM.mobileno +this.state.reqPDAM.productid+password);
        let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
        let reqPDAMNew = {...this.state.reqPDAM};
        reqPDAMNew['signature'] = ReSign;
        console.log("REQ PDAM: ",reqPDAMNew);
        // hit API
        axios.post(REACT_APP_URL_API+'MobileInq',reqPDAMNew).then((res) => {
          console.log("RES PDAM: ",res.data);
          if(res.data.rc === "00"){
            swal.close();
            const addDatas = JSON.parse(res.data.additionaldata);
            this.props.history.push('/detilpembayaran',{
              billerid : this.state.reqPDAM.billerid,
              productid : this.state.reqPDAM.productid,
              userid : this.state.reqPDAM.userid,
              partnerid : this.state.reqPDAM.partnerid,
              publickey : this.state.reqPDAM.publickey,
              customerid : res.data.customerid,
              customername : res.data.customername,
              totalamount : res.data.totalamount,
              additionaldata : res.data.additionaldata,
              addDatas : addDatas,
              mobileno : this.state.reqPDAM.mobileno,
              email : this.state.reqPDAM.email,
              extendinfo : res.data.extendinfo,
              trackingref : res.data.trackingref,
              username : this.state.terminalid,
              flag : "",
              img_detail : res.data.imgpth,
              namaproduct : res.data.title,
              n_product_detail : res.data.title,
              link : this.state.link,
            });    
          }else{
            swal("Oops..", res.data.rcdesc , "warning");
          }
        }, (err) => {
          swal("Error", err.message , "error");
        })
        .catch((error) => {
          // console.log(error.message);
          swal("Error", error.message , "error");
        })
      }else{
        if(!this.state.reqPDAM.productid){
          swal("Oops..", "Pilih salah satu product." , "warning");
        }else if(!this.state.reqPDAM.customerid){
          swal("Oops..", "No Pelanggan harus diisi." , "warning");
        }
      }
    }
  
    render(){
      return(
        <>
        <div className="navbarmenu">
          <Link to={this.state.windowURL} className="menu-barsmenu">
            <FaIcons.FaArrowLeft /> 
          </Link>
          <h2> {this.state.nama} </h2>
        </div>  
        {/* <div className="jumbotron"></div> */}
        <div className="container-fluid main-menu">
          <div className="container2">
            <div className="h4">Pilih Partner</div>
          </div>
          <Dropdown
            placeholder='Select Partner'
            fluid
            search
            selection
            options={this.state.planets}
            onChange={this.handleChange}
          />
          <div className="container2">
            <div className="h4">No. Pelanggan</div>
            <div><input className="input" type="number" id="customerid" min="0" name="customerid" placeholder="Nomor Pelanggan" onChange={this.handleChange} /></div>
          </div>
        </div>
        <div className="container-fluid button1" > 
          <hr/>
          <b.Button variant="success" name="btnproses" id="btnproses" onClick={this.postdatatoAPI} block>Continue</b.Button>
        </div>
        </>
      );
    };
  }
  
  export default Pdam;