import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';
import 'react-smarttab/dist/index.css';
import NumberFormat from 'react-number-format';

const { REACT_APP_URL_API } = process.env;

class Aktivitas extends Component {

  constructor(props){
    super(props);
    this._isMounted = false;
    this.state = {
      nama : props.nama,
      link : props.link,
      prev_periode : "",
      publickey : props.publickey,
      dataAktif : [],
      dataPeriode : [],
      listMetodeByr : [],
      terminalid : props.terminalid,
      windowURL : props.windowURL,
      token_struct : props.history.location.state.token_struct,
      rincianAktivitas : {
        title : props.history.location.state.title,
        orderid : props.history.location.state.orderid,
        bookid : props.history.location.state.bookid,
        customername : props.history.location.state.customername,
        totalamount : props.history.location.state.totalamount,
        datetrans : props.history.location.state.datetrans,
        no_pel : props.history.location.state.no_pel,
        status : props.history.location.state.status,
        paymentmetode : props.history.location.state.paymentmetode,
        version : "1",
        imagpth : props.history.location.state.imagpth,
      },
      reqRinAktivitas : {
        HEADER : header,
        signature : "",
        flag : props.history.location.state.paymentmetode,
        version : "1",
        partnerid : props.partnerid,
        userid : props.userid,
        username : props.terminalid,
        publickey : props.publickey,
      },
      reqInvoice : {
        HEADER: header,
        bookid: props.history.location.state.bookid,
        userid: props.userid,
        partnerid: props.partnerid,
        publickey: props.publickey,
        productid: props.history.location.state.productid,
        billerid: props.history.location.state.billerid,
        signature: "",
      }
    };

    if(this.state.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }
  }

  componentDidMount(){
    // console.log("TOKEN STRUCK (RINCIAN AKTIVITAS): ", this.state.token_struct);
    this._isMounted = true;
    document.body.style.background = "white";
    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });
  }

  postdatatoAPI = () => {
    swal({
      text: "Mohon Tunggu",
      icon: require("./icon/loading3.gif"),
      buttons: false,
      closeOnClickOutside : false
    });
    let listMetodeByrNew = [];
    // Generate Signature md5
    var md5 = require('md5');
    let signature = md5(header + this.state.reqRinAktivitas.userid  + this.state.reqRinAktivitas.partnerid + this.state.reqRinAktivitas.publickey + this.state.reqRinAktivitas.flag + this.state.reqRinAktivitas.version  + password);
    // let signature = md5(header + this.state.reqRinAktivitas.flag + this.state.reqRinAktivitas.version + password);
    let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
    let reqRinAktivitasNew = {...this.state.reqRinAktivitas};
    reqRinAktivitasNew['signature'] = ReSign;
    console.log("REQ RINCIAN AKTIVITAS: ", reqRinAktivitasNew);

    axios.post(REACT_APP_URL_API + 'Getmethodepay',reqRinAktivitasNew).then(response => {
      console.log("RES RINCIAN AKTIVITAS :", response.data);
      return response.data;
    }).then(data => {
      if(data.rc === "00"){
        swal.close();
        const strojb = JSON.parse(data.strobj);
        // console.log("DATA: ", strojb);
        // console.log("ISI DATA: ", strojb.ls_mp[0]);
        // console.log("DATA LENGTH: ", strojb.ls_mp.length);
        for(let x=0; x < strojb.ls_mp.length; x++){
          listMetodeByrNew[x] = { item_1 : strojb.ls_mp[x].item_1, item_2 : strojb.ls_mp[x].item_2, item_3 : strojb.ls_mp[x].item_3 }
        }
        if (this._isMounted) {
          this.setState({
            listMetodeByr: listMetodeByrNew,
          });
        }
        // console.log("LIST METODE BAYAR: ", this.state.listMetodeByr);
        this.props.history.push('/metodepembayaran',{
          billerid : this.state.rincianAktivitas.billerid,
          productid : this.state.rincianAktivitas.productid,
          userid : this.state.reqRinAktivitas.userid,
          partnerid : this.state.reqRinAktivitas.partnerid,
          publickey : this.state.reqRinAktivitas.publickey,
          customerid : this.state.terminalid,
          customername : this.state.rincianAktivitas.customername,
          totalamount : this.state.rincianAktivitas.totalamount,
          additionaldata : "",
          mobileno : this.state.rincianAktivitas.no_pel,
          email : "",
          extendinfo : "",
          trackingref : "",
          username : this.state.terminalid,
          flag : "",
          img_detail : this.state.rincianAktivitas.imagpth,
          namaproduct : this.state.rincianAktivitas.title,
          n_product_detail : this.state.rincianAktivitas.title,
          listMetodeBayar : this.state.listMetodeByr,
          bookingid : this.state.rincianAktivitas.bookid,
          orderid : this.state.rincianAktivitas.orderid
        });
      }else{
        swal("Oops..", data.rcdesc , "warning");
      }
    }).catch((error) => {
      // console.log(error.message);
      swal({title: error.message, text: "Kembali ke halaman sebelumnya.", icon: "error"}).then(function() {
        // window.location = "./solusipayweb";
      });
    });

  }

  lihatStruk= (tanggallunas, noInvoice, noResi, namaProduk, idPel, nama, ref, tagihan, totalBayar) =>{
    swal({
      text: "Mohon Tunggu",
      icon: require("./icon/loading3.gif"),
      buttons: false,
      closeOnClickOutside : false
    });
    // Generate Signature md5
    var md5 = require('md5');
    let signature = md5(header + this.state.reqInvoice.userid  + this.state.reqInvoice.partnerid + this.state.reqInvoice.publickey + this.state.reqInvoice.billerid + this.state.reqInvoice.bookid  + password);
    let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
    let reqInvoiceNew = {...this.state.reqInvoice};
    reqInvoiceNew['signature'] = ReSign;
    console.log("REQ INVOICE: ", reqInvoiceNew);

    axios.post(REACT_APP_URL_API + 'invoice',reqInvoiceNew).then(response => {
      console.log("RES INVOICE :", response.data);
      return response.data;
    }).then(data => {
      if(data.rc === "00"){
        swal.close();
        //TV
        // additionaldata: "[{"label":"PEMBAYARAN","value":"BIG TV","required":"mandatory","type":"deskripsi"},{"label":"No Resi","value":"339062776","required":"mandatory","type":"text"},{"label":"Nama Produk","value":"TVBIG","required":"mandatory","type":"text"},{"label":"ID Pel","value":"1072161447","required":"mandatory","type":"text"},{"label":"Nama","value":"Mr. syahrun as ","required":"mandatory","type":"text"},{"label":"REFF","value":"CLOSE PAYMENT ","required":"mandatory","type":"text"},{"label":"Tagihan","value":"Rp138.499","required":"mandatory","type":"text"},{"label":"Total Bayar","value":"Rp138.499","required":"mandatory","type":"textb"},{"label":"MOHON STRUK INI DISIMPAN SEBAGAI BUKTI PEMBAYARAN YANG SAH","value":"","required":"mandatory","type":"pesan"},{"label":"TERIMA KASIH","value":"","required":"mandatory","type":"tunggakan"},{"label":"","value":"","required":"mandatory","type":"spesial"}]"

        //PLN
        //"additionaldata":"[{\"label\":\"PEMBAYARAN\",\"value\":\"PLN POSTPAID\",\"required\":\"mandatory\",\"type\":\"deskripsi\"},{\"label\":\"IDPEL\",\"value\":\"534310259702\",\"required\":\"mandatory\",\"type\":\"text\"},{\"label\":\"NAMA\",\"value\":\"NAMA. /CH@%\\u0027M$,^\\u0026 T\\\"ST#03\",\"required\":\"mandatory\",\"type\":\"text\"},{\"label\":\"TARIF/DAYA\",\"value\":\"S2/000000900VA\",\"required\":\"mandatory\",\"type\":\"text\"},{\"label\":\"BL/TH\",\"value\":\"APR2016\",\"required\":\"mandatory\",\"type\":\"text\"},{\"label\":\"STAND METER\",\"value\":\"01239800-01250700\",\"required\":\"mandatory\",\"type\":\"text\"},{\"label\":\"RP TAG PLN\",\"value\":\"Rp49.940\",\"required\":\"mandatory\",\"type\":\"text\"},{\"label\":\"PLN MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH\",\"value\":\"\",\"required\":\"mandatory\",\"type\":\"pesan\"},{\"label\":\"ADMIN BANK\",\"value\":\"Rp2.500\",\"required\":\"mandatory\",\"type\":\"text\"},{\"label\":\"TOTAL BAYAR\",\"value\":\"Rp52.440\",\"required\":\"mandatory\",\"type\":\"textb\"},{\"label\":\"\",\"value\":\"\",\"required\":\"mandatory\",\"type\":\"spesial\"}]"

        const addDatas = JSON.parse(data.additionaldata);
        // console.log(addDatas);
        // console.log(addDatas[0].value);
        // let noresi = addDatas.filter(addData => addData.label === "No Resi");
        // let namaproduk = addDatas.filter(addData => addData.label === "Nama Produk");
        // let idpel = addDatas.filter(addData => addData.label === "ID Pel");
        // let nama = addDatas.filter(addData => addData.label === "Nama");
        // let ref = addDatas.filter(addData => addData.label === "REFF");
        // let tagihan = addDatas.filter(addData => addData.label === "Tagihan");
        // let totalbayar = addDatas.filter(addData => addData.label === "Total Bayar");
        // console.log("no resi : ", noresi);
        // console.log("no resi value : ", noresi[0].value);
        this.props.history.push('/struk',{
          tanggallunas : data.tanggallunas,
          noInvoice : data.nomorinvoice,
          additionaldata : addDatas,
          imgpth : data.imgpth,
          token_struct : this.state.token_struct,
          // noResi : noresi[0].value,
          // namaProduk : namaproduk[0].value,
          // idPel : idpel[0].value,
          // nama : nama[0].value,
          // ref : ref[0].value,
          // tagihan : tagihan[0].value,
          // totalBayar : totalbayar[0].value,
          // additionaldata : addDatas,
          // imgpth : data.imgpth,
        });
      }else{
        swal("Oops..", data.rcdesc , "warning");
      }
    }).catch((error) => {
      // console.log(error.message);
      swal({title: error.message, text: "Kembali ke halaman sebelumnya.", icon: "error"}).then(function() {
        // window.location = "./solusipayweb";
      });
    });
  }

  render(){
    return (
      <>
      <div className="navbarmenu">
        <Link to="/aktivitas" className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>
      <br></br>
      <div className="container-fluid" > 
        <div className="container2">
          <div>
            <img src={require("../"+ this.state.rincianAktivitas.imagpth)} alt="" className="iconDetails7" />
          </div>
          <div className="h5">{this.state.rincianAktivitas.orderid}</div>
          <div className="h5">{this.state.rincianAktivitas.customername}</div>
          <div className="h5"><NumberFormat value={this.state.rincianAktivitas.totalamount} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
        </div>
        <hr></hr>
        <div className="rows3">
          <div className="title">Tanggal Transaksi</div>
          <div className="sub_title">{this.state.rincianAktivitas.datetrans}</div>
        </div>
        <div className="rows3">
          <div className="title">Nomor Pelanggan</div>
          <div className="sub_title">{this.state.rincianAktivitas.no_pel}</div>
        </div>
        <div className="rows3">
          <div className="title">Status</div>
          <div className="sub_title">{this.state.rincianAktivitas.status}</div>
        </div>
      </div>
      <div className="container-fluid button1" > 
        {
          this.state.rincianAktivitas.status === "Menunggu Transfer" ? (
            <b.Button variant="success" name="btnproses" id="btnproses" onClick={this.postdatatoAPI} block>Change Payment</b.Button>
          ):this.state.rincianAktivitas.status === "Sukses Terbayar" ? (
            <b.Button variant="success" name="btnStruck" id="btnStruck" onClick={this.lihatStruk} block>Lihat Struck</b.Button>
          ):(
            null
          ) 
        }
      </div>
      </>
    );
  }
}

export default Aktivitas;
