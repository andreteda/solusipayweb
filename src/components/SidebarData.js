import React from 'react';
import * as AiIcons from 'react-icons/ai';

export const SidebarData = [
  {
    title: 'Beranda',
    path: '/solusipayweb',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },
  {
    title: 'Aktivitas',
    path: '/aktivitas',
    icon: <AiIcons.AiOutlinePaperClip />,
    cName: 'nav-text'
  }
];