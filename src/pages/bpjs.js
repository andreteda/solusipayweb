import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';

const { REACT_APP_URL_API } = process.env;

const periodeBulan = [
  { label: "-Pilih Periode-", value: "" },
  { label: "1 Bulan", value: "1" },
  { label: "2 Bulan", value: "2" },
  { label: "3 Bulan", value: "3" },
  { label: "4 Bulan", value: "4" },
  { label: "5 Bulan", value: "5" },
  { label: "6 Bulan", value: "6" },
  { label: "7 Bulan", value: "7" },
  { label: "8 Bulan", value: "8" },
  { label: "9 Bulan", value: "9" },
  { label: "10 Bulan", value: "10" },
  { label: "11 Bulan", value: "11" },
  { label: "12 Bulan", value: "12" }
];

class Bpjs extends Component{

  constructor(props){
    super(props);
    this.state = {
      nama : props.nama,
      link : props.link,
      terminalid : props.terminalid,
      windowURL : props.windowURL,
      reqBPJS : {
        HEADER : header,
        billerid : props.billerid,
        customerid : "",
        userid : props.userid,
        partnerid : props.partnerid,
        publickey : props.publickey,
        email : props.email,
        extendinfo : "1",
        mobileno : props.mobileno,
        productid : "001",
        signature : ""
      }
    };

    if(this.state.reqBPJS.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }
  }

  handleChange = (event) => {
    let reqBPJSNew = {...this.state.reqBPJS}; //mengcopy variable dengan isi sama
    reqBPJSNew[event.target.name] = event.target.value;
    this.setState({
      reqBPJS: reqBPJSNew
    })
  }

  componentDidMount() {
    document.body.style.background = "white";
    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });
  }

  postdatatoAPI = () => {
    
    if(this.state.reqBPJS.customerid)
    {
      swal({
        text: "Mohon Tunggu",
        icon: require("./icon/loading3.gif"),
        buttons: false,
        closeOnClickOutside : false
      });
      // Create Signature
      var md5 = require('md5');
      let signature = md5(header+this.state.reqBPJS.userid+this.state.reqBPJS.partnerid+this.state.reqBPJS.publickey+this.state.reqBPJS.billerid+this.state.reqBPJS.customerid+this.state.reqBPJS.mobileno +this.state.reqBPJS.productid+password);
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      let reqBPJSNew = {...this.state.reqBPJS};
      reqBPJSNew['signature'] = ReSign;
      console.log("REQ BPJS: ", reqBPJSNew);
      // //axios.post('http://117.54.12.140:8080/SolusiBiller/webresources/register',this.state.reqPLN).then((res) => {
      axios.post(REACT_APP_URL_API+'MobileInq',reqBPJSNew).then((res) => {
        console.log("RES BPJS: ",res.data);
        if(res.data.rc === "00"){
          swal.close();
          const addDatas = JSON.parse(res.data.additionaldata);
          this.props.history.push('/detilpembayaran',{
            billerid : this.state.reqBPJS.billerid,
            productid : this.state.reqBPJS.productid,
            userid : this.state.reqBPJS.userid,
            partnerid : this.state.reqBPJS.partnerid,
            publickey : this.state.reqBPJS.publickey,
            customerid : res.data.customerid,
            customername : res.data.customername,
            totalamount : res.data.totalamount,
            additionaldata : res.data.additionaldata,
            addDatas : addDatas,
            mobileno : this.state.reqBPJS.mobileno,
            email : this.state.reqBPJS.email,
            extendinfo : res.data.extendinfo,
            trackingref : res.data.trackingref,
            username : this.state.terminalid,
            flag : "",
            img_detail : res.data.imgpth,
            namaproduct : res.data.title,
            n_product_detail : res.data.title,
            link : this.state.link,
          });      
        }else{
          swal("Oops..", res.data.rcdesc , "warning");
        }
      }, (err) => {
        swal("Error", err.message , "error");
      })
      .catch((error) => {
        // console.log(error.message);
        swal("Error", error.message , "error");
      })
    }else{
      swal("Oops..", "No Pelanggan harus diisi." , "warning");
    }
  }

  render(){
    return(
      <>
      <div className="navbarmenu">
        <Link to={this.state.windowURL} className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>  
      {/* <div className="jumbotron"></div> */}
      <div className="container-fluid main-menu">
        <div className="container2">
          <div>
            <div className="h4">No. Pelanggan</div>
            <div><input className="input" type="number" id="customerid" min="0" name="customerid" placeholder="Nomor Pelanggan" onChange={this.handleChange} /></div>
          </div>
        </div>
       
        <b.Row>
          <b.Col>
            <b.Form.Label>
              Jumlah Periode
            </b.Form.Label>
          </b.Col>
          <b.Col>
            <b.FormGroup>
              <b.Form.Control as="select" className="select" name="extendinfo" id="extendinfo" onChange={this.handleChange}>
                {periodeBulan.map((item, index) => {
                    return (
                      <option key={index} value={item.value} >
                        {item.label}
                      </option>
                    )
                })}
              </b.Form.Control>
            </b.FormGroup>
          </b.Col>
        </b.Row>
      </div>
      <div className="container-fluid button1" > 
        <hr/>
        <b.Button variant="success" name="btnproses" id="btnproses" onClick={this.postdatatoAPI} block>Continue</b.Button>
      </div>
      </>
    );
  };
}

export default Bpjs;