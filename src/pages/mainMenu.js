import React, {Component} from 'react';
import { Row, Figure} from 'react-bootstrap';
import { Link} from 'react-router-dom';
// import { MainMenuData } from './data/mainMenu';
import './components/mainMenu.css';
import Navbar from '../components/Navbar';
// import { DropdownDivider } from 'semantic-ui-react';

class mainMenu extends Component{
  constructor(props){
    super(props);
    this.state = {
      showMenu : true,
      data : props.data,
    };

    this.hideMenu = this.hideMenu.bind(this);
  }

  componentDidMount() {
    document.body.style.background = "rgb(216,216,216)";
  }

  hideMenu(){
    this.setState ((state,props) => {
      return {
        showMenu : false
      };
    });
  }

  render(){
    return (
      <>
        <Navbar />
        {
          this.state.data.filter(data => data.biller_id === "4" || data.biller_id === "12").length === 0 ? (
            null
          ):(
            <>
            <div className="rowsBG"></div>
            <div className="container-fluid main-menu">
              Isi Pulsa
              <hr className="garisMenu"></hr>
              <div className="text-center row">
                {this.state.data.filter(data => data.biller_id === "4" || data.biller_id === "12").map((item, index) => {
                  return (
                    <Link to={'/'+item.pages_name_in_apps.toString().toLowerCase()} onClick={this.hideMenu} className="column2" key={index}>
                      <Figure className="figure">
                        <Figure.Image src={require('../'+item.icon)} className="iconMenu" />
                        <Figure.Caption>{item.biller_name}</Figure.Caption>
                      </Figure>
                    </Link>
                  )
                })}
              </div>
            </div>
            </>
          )
        }
        {
          this.state.data.filter(data => data.biller_id === "1" || data.biller_id === "2" || data.biller_id === "3" || data.biller_id === "7" || data.biller_id === "10" || data.biller_id === "16").length === 0 ? (
            null
          ):(
            <>
            <div className="rowsBG"></div>
            <div className="container-fluid main-menu">
              Tagihan
              <hr className="garisMenu"></hr>
              <div className="text-center row">
                {this.state.data.filter(data => data.biller_id === "1" || data.biller_id === "2" || data.biller_id === "3" || data.biller_id === "7" || data.biller_id === "10" || data.biller_id === "16" || data.biller_id === "14" ).map((item, index) => {
                  return (
                      <Link to={'/'+item.pages_name_in_apps.toString().toLowerCase()} onClick={this.hideMenu} className="column2" key={index} >
                        <Figure className="figure">
                          <Figure.Image src={require('../'+item.icon)} className="iconMenu" />
                          <Figure.Caption>{item.biller_name}</Figure.Caption>
                        </Figure>
                      </Link>
                  )
                })}
              </div>
            </div>
            </>
          )
        }
        {
          this.state.data.filter(data => data.biller_id === "9" || data.biller_id === "11").length === 0 ? (
            null
          ):(
            <>
            <div className="rowsBG"></div>
            <div className="container-fluid main-menu">
              Asuransi Pribadi
              <hr className="garisMenu"></hr>
              <div className="text-center row">
                {this.state.data.filter(data => data.biller_id === "9" || data.biller_id === "11").map((item, index) => {
                  return (
                      <Link to={'/'+item.pages_name_in_apps.toString().toLowerCase()} onClick={this.hideMenu} className="column2" key={index}>
                        <Figure className="figure">
                          <Figure.Image src={require('../'+item.icon)} className="iconMenu" />
                          <Figure.Caption>{item.biller_name}</Figure.Caption>
                        </Figure>
                      </Link>
                  )
                })}
              </div>
            </div>
            </>
          )
        }
        {
          this.state.data.filter(data => data.biller_id === "13" || data.biller_id === "15").length === 0 ? (
            null
          ):(
            <>
            <div className="rowsBG"></div>
            <div className="container-fluid main-menu">
              Hiburan
              <hr className="garisMenu"></hr>
              <Row className="text-center">
                {this.state.data.filter(data => data.biller_id === "13" || data.biller_id === "15").map((item, index) => {
                  return (
                      <Link to={'/'+item.pages_name_in_apps.toString().toLowerCase()} onClick={this.hideMenu} className="column2" key={index}>
                        <Figure className="figure">
                          <Figure.Image src={require('../'+item.icon)} className="iconMenu" />
                          <Figure.Caption>{item.biller_name}</Figure.Caption>
                        </Figure>
                      </Link>
                  )
                })}
              </Row>
            </div>
            </>
          )
        }
      </>
    );
  };
}

export default mainMenu;