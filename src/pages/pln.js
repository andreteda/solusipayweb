import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import { dataTokenListrik } from './data/dataTokenListrik';
import axios from 'axios';
import swal from 'sweetalert';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';
import './components/tab.css';

const { REACT_APP_URL_API } = process.env;

class Pln extends Component{
  constructor(props){
    super(props);
    this.state = {
      statusToken : true, 
      statusTagihan: false,
      nama: props.nama,
      link : props.link,
      windowURL : props.windowURL,
      terminalid : props.terminalid,
      reqPLN : {
        HEADER : header,
        billerid : props.billerid,
        customerid : "",
        userid : props.userid,
        partnerid : props.partnerid,
        publickey : props.publickey,
        email : props.email,
        extendinfo : "0",
        mobileno : props.mobileno,
        productid : "",
        signature : ""
      }
    };

    this.tokenlistrik = this.tokenlistrik.bind(this);
    this.tagihanlistrik = this.tagihanlistrik.bind(this);

    if(this.state.reqPLN.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }
  }

  componentDidMount(){
    document.body.style.background = "white";
    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });
    // console.log(this.state.reqPLN.publickey);
  }

  tokenlistrik(){
    this.setState ((state,props) => {
      return {
        statusToken : true,
        statusTagihan : false
      };
    });
  }

  handleChange = (event) => {
    let reqPLNNew = {...this.state.reqPLN}; //mengcopy variable dengan isi sama
    reqPLNNew[event.target.name] = event.target.value;
    console.log(reqPLNNew);
    this.setState({
      reqPLN: reqPLNNew
    })

    // if(reqPLNNew['nominal']){
    //   this.postdatatoAPI();
    // }
  }

  postdatatoAPI = (value) => {
    let reqPLNNew = {...this.state.reqPLN};
    var price = 0;
    if(this.state.statusToken === true){
      reqPLNNew['productid'] = "001";
      price = value;
    }else{
      reqPLNNew['productid'] = "002";
      price = value;
    }

    if(reqPLNNew.customerid)
    {
      swal({
        text: "Mohon Tunggu",
        icon: require("./icon/loading3.gif"),
        buttons: false,
        closeOnClickOutside : false
      });
      // Create Signature
      var md5 = require('md5');
      let signature = md5(header+this.state.reqPLN.userid+this.state.reqPLN.partnerid+this.state.reqPLN.publickey+this.state.reqPLN.billerid+this.state.reqPLN.customerid+this.state.reqPLN.mobileno +reqPLNNew.productid+password);
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      let reqPLNNew2 = {...reqPLNNew};
      reqPLNNew2['signature'] = ReSign;
      console.log("REQ PLN: ",reqPLNNew2)
      axios.post(REACT_APP_URL_API+'MobileInq',reqPLNNew2).then((res) => {
        console.log("RES PLN: ",res.data);
        if(res.data.rc === "00"){
          swal.close();
          const addDatas = JSON.parse(res.data.additionaldata);
          var Harga = 0;
          if(reqPLNNew['productid'] === "001"){
            Harga = parseInt(price) + parseInt(addDatas.Adminvalue);
          }else{
            Harga = parseInt(res.data.totalamount) + parseInt(addDatas.Adminvalue);
          }
          this.props.history.push('/detilpembayaran',{
            billerid : reqPLNNew2.billerid,
            productid : reqPLNNew2.productid,
            userid : reqPLNNew2.userid,
            partnerid : reqPLNNew2.partnerid,
            publickey : reqPLNNew2.publickey,
            customerid : res.data.customerid,
            customername : res.data.customername,
            totalamount : Harga,
            additionaldata : res.data.additionaldata,
            addDatas : addDatas,
            mobileno : reqPLNNew2.mobileno,
            email : reqPLNNew2.email,
            extendinfo : res.data.extendinfo,
            trackingref : res.data.trackingref,
            username : this.state.terminalid,
            flag : "",
            img_detail : res.data.imgpth,
            namaproduct : res.data.title,
            n_product_detail : res.data.title,
            link : this.state.link,
          });  
        }else{
          swal("Oops..", res.data.rcdesc , "warning");
        }
      }, (err) => {
        swal("Error", err.message , "error");
      })
      .catch((error) => {
        swal("Error", error.message , "error");
      })
    }else{
      swal("Oops..", "No Pelanggan harus diisi." , "warning");
    }
  }

  tagihanlistrik(){
    this.setState ((state,props) => {
      return {
        statusToken : false,
        statusTagihan : true
      };
    });
  }

  render(){
    return(
      <>
      <div className="navbarmenu">
        <Link to={this.state.windowURL} className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>  
      {/* <div className="jumbotron"></div> */}
      <div className="container-fluid main-menu">
        <div className="containertab">
            <button type="button" className={this.state.statusToken ? 'plntab' : 'plntab-non'} onClick={this.tokenlistrik}>  Token Listrik</button>
            <button type="button" className={this.state.statusToken ? 'plntab-non' : 'plntab'} onClick={this.tagihanlistrik}>  Tagihan Listrik</button>
        </div>
        <div className="container2">
          <div className="h4">No. Pelanggan</div>
          <div><input className="input" type="number" id="customerid" min="0" name="customerid" placeholder="Nomor Pelanggan" onChange={this.handleChange} /></div>
        </div>
      </div>
      {
        this.state.statusToken === true ? 
        (
          <div className="container-fluid" >
            <div className="h4">Nominal</div>
            {/* <b.ListGroup variant="flush"> */}
              {dataTokenListrik.map((item, index) => {
                  return (
                    <button type="button" key={item.value}  className="nominal" onClick={this.postdatatoAPI.bind(this, item.value)}>{item.label}</button>
                    // <b.ListGroup.Item action key={item.value} onClick={this.postdatatoAPI.bind(this, item.value)} name="nominal" id="nominal" value={item.value}>
                    //   {item.label}
                    // </b.ListGroup.Item>
                  )
              })}
            {/* </b.ListGroup> */}
          </div>
        ) : (
          <div className="container-fluid main-menu button1" > 
            <hr/>
            <b.Button variant="success" name="btnproses" id="btnproses" onClick={this.postdatatoAPI.bind(this, 0)} block>Continue</b.Button>
          </div>
        )
      }
      </>
    );
  };
}

export default Pln;