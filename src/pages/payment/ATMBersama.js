import React, {} from 'react';
import * as b from 'react-bootstrap';

function ATMBersama() {
  return (
    <>
      <b.Accordion defaultActiveKey="0">
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="0">
            ATM Bank Lain
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="0">
            <b.Card.Body>
              1. Masukan kartu ATM & PIN Anda. <br></br>
              2. Pilih menu TRANSFER > TRANSFER KE BANK LAIN. <br></br>
              3. Masukkan kode Bank Permata : 013. <br></br>
              4. Masukkan kode perusahaan SolusiPay (701) diikuti dengan dengan nomor Virtual Account Anda. Contoh : 70108571234567. <br></br>
              5. Masukkan nominal sesuai dengan tagihan. <br></br>
              6. Ikuti instruksi selanjutnya. <br></br>
              7. Setelah pembayaran sukses, simpan bukti pembayaran Anda.
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="1">
            Mobile & Internet
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="1">
            <b.Card.Body>
              1. Log in ke internet banking / aplikasi mobile banking Anda. <br></br>
              2. Pilih TRANSFER > PERMATA BANK / masukkan kode 013. <br></br>
              3. Masukkan kode perusahaan SolusiPay (701) diikuti dengan dengan nomor Virtual Account Anda. Contoh : 70108571234567. <br></br>
              4. Masukkan nominal sesuai dengan tagihan Anda. <br></br>
              5. Ikuti instruksi selanjutnya. <br></br>
              6. Masukkan PIN Mandiri Mobile Anda lalu tekan OK <br></br>
              7. Setelah pembayaran sukses, simpan bukti pembayaran Anda
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
      </b.Accordion>
    </>
  )
}

export default ATMBersama;
