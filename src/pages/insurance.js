import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import { Dropdown } from 'semantic-ui-react';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';

const { REACT_APP_URL_API } = process.env;

class Insurance extends Component{

    constructor(props){
      super(props);
      this._isMounted = false;
      this.state = {
        nama : props.nama,
        link : props.link,
        windowURL : props.windowURL,
        terminalid : props.terminalid,
        fl_Inq : "0",
        namaproduk : "",
        amount: "",
        imgdetail: "",
        reqInsurance : {
          HEADER : header,
          billerid : props.billerid,
          customerid : "",
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          email : props.email,
          extendinfo : "0",
          mobileno : props.mobileno,
          productid : "",
          signature : ""
        },
        reqDataInsurance : {
          HEADER : header,
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          billerId : props.billerid,
          signature : ""
        },
        planets: [],
      };

      if(this.state.reqInsurance.publickey === "" && this.state.windowURL === ""){
        swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
      }
    }

    componentDidMount() {
      document.body.style.background = "white";
      let windowURLNew = './solusipayweb/'+ this.state.windowURL;
      this.setState({
        windowURL: windowURLNew,
      });
      this._isMounted = true;
      // Generate Signature md5
      var md5 = require('md5');
      let signature = md5(header+this.state.reqDataInsurance.userid+this.state.reqDataInsurance.partnerid+this.state.reqDataInsurance.publickey+this.state.reqDataInsurance.billerId+password);
      // Buat Variable baru untuk mengUpdate Request Data MF
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      let reqDataInsuranceNew = {...this.state.reqDataInsurance};
      reqDataInsuranceNew['signature'] = ReSign;
      console.log("REQ DATA INSURANCE: ", reqDataInsuranceNew);
      // Hit API POST 
      let initialPlanets = [];
        axios.post(REACT_APP_URL_API + 'ListProduct',reqDataInsuranceNew).then(response => {
          console.log("RES DATA INSURANCE: ", response.data.data);
          return response.data.data;
      }).then(data => {
        for (let i = 0; i < data.length; i++) {
          initialPlanets[i] = {key: data[i].productid, value: data[i].productid, image: require("../"+data[i].img), text: data[i].productdesc, groupid: data[i].groupid, path_img: data[i].img};
        }
        // console.log(initialPlanets);
        if (this._isMounted) {
          this.setState({
            planets: initialPlanets,
          });
        }
      }).catch((error) => {
        // console.log(error.message);
        swal({title: "ERROR - Server sedang maintenance.", text: "Kembali ke halaman utama.", icon: "error"}).then(function() {
          window.location = "./solusipayweb";
        });
      });
    }

    componentWillUnmount() {
      this._isMounted = false;
    }

    handleChange = (event,data) => {
      let reqInsuranceNew = {...this.state.reqInsurance}; //mengcopy variable dengan isi sama
      let arr_groupid = [];
      let arr_namaproduk = [];
      let arr_imgdetail = [];
      let groupid = "0";
      let namaproduk = "";
      let imgdetail = "";
      if (data){
        // console.log(data);
        reqInsuranceNew['productid'] = data.value;
        arr_groupid = data.options.filter(dataOpt => dataOpt.key === data.value);
        arr_namaproduk = data.options.filter(dataOpt => dataOpt.key === data.value);
        arr_imgdetail = data.options.filter(dataOpt => dataOpt.key === data.value);
        groupid = arr_groupid[0].groupid;
        namaproduk = arr_namaproduk[0].text;
        imgdetail = arr_imgdetail[0].path_img;
        // console.log("groupid: ", groupid);
        this.setState({
          fl_Inq : groupid,
          namaproduk : namaproduk,
          imgdetail : imgdetail,
        });
      }else{
        if(this.state.fl_Inq ==="1"){
          reqInsuranceNew[event.target.name] = event.target.value;
          this.setState({
            amount : event.target.value,
          });
        }else{
          // console.log(event.target.name,event.target.value);
          reqInsuranceNew[event.target.name] = event.target.value;
        }
      }
      this.setState({
        reqInsurance: reqInsuranceNew
      })
    }
  
    postdatatoAPI = () => {
      if(this.state.fl_Inq === "1"){
        if(this.state.reqInsurance.customerid !== "" && this.state.reqInsurance.productid !== "" && this.state.amount !== "0" && this.state.amount !== "")
        {
          let data = "{\"Flagaddtional\":\"1\",\"Adminvalue\":\"0\",\"Ls_Resadd\":[{\"label\":\"Nama Produk\",\"value\":\"" + this.state.namaproduk + " \",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"ID Pel\",\"value\":\"" + this.state.reqInsurance.customerid + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Nama\",\"value\":\"" + this.state.terminalid + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Tagihan\",\"value\":\"Rp " + this.state.amount + "\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"},{\"label\":\"Biaya Admin\",\"value\":\"Rp 0\",\"required\":\"mandatory\",\"type\":\"text\",\"jumspasi\":\"0\"}]}"
    
          let trckreff = Math.floor(Math.random() * 1000000).toString();
          let trck = 'SLS' + trckreff + this.state.reqInsurance.mobileno;
          const addDatas = JSON.parse(data);
          this.props.history.push('/detilpembayaran',{
            billerid : this.state.reqInsurance.billerid,
            productid : this.state.reqInsurance.productid,
            userid : this.state.reqInsurance.userid,
            partnerid : this.state.reqInsurance.partnerid,
            publickey : this.state.reqInsurance.publickey,
            customerid : this.state.reqInsurance.customerid,
            customername : this.state.terminalid,
            totalamount : this.state.amount,
            additionaldata : data,
            addDatas : addDatas,
            mobileno : this.state.reqInsurance.mobileno,
            email : this.state.reqInsurance.email,
            extendinfo : this.state.reqInsurance.extendinfo,
            trackingref : trck,
            username : this.state.terminalid,
            flag : "",
            img_detail : this.state.imgdetail,
            namaproduct : this.state.nama,
            n_product_detail : this.state.namaproduk,
            link : this.state.link,
          });
        }else{
          if(!this.state.reqInsurance.productid){
            swal("Oops..", "Pilih salah satu product." , "warning");
          }else if(!this.state.reqInsurance.customerid){
            swal("Oops..", "No Pelanggan harus diisi." , "warning");
          }else if(!this.state.nominal || this.state.nominal === "0"){
            swal("Oops..", "Nominal harus diisi." , "warning");
          }
        }
      }else{
        if(this.state.reqInsurance.customerid && this.state.reqInsurance.productid)
        {
          swal({
            text: "Mohon Tunggu",
            icon: require("./icon/loading3.gif"),
            buttons: false,
            closeOnClickOutside : false
          });
          // Create Signature
          var md5 = require('md5');
          let signature = md5(header+this.state.reqInsurance.userid+this.state.reqInsurance.partnerid+this.state.reqInsurance.publickey+this.state.reqInsurance.billerid+this.state.reqInsurance.customerid+this.state.reqInsurance.mobileno +this.state.reqInsurance.productid+password);
          let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
          let reqInsuranceNew = {...this.state.reqInsurance};
          reqInsuranceNew['signature'] = ReSign;
          console.log("REQ INSURANCE: ", reqInsuranceNew);
          // hit API
          axios.post(REACT_APP_URL_API+'MobileInq',reqInsuranceNew).then((res) => {
            console.log("RES INSURANCE: ",res.data);
            if(res.data.rc === "00"){
              swal.close();
              const addDatas = JSON.parse(res.data.additionaldata);
              this.props.history.push('/detilpembayaran',{
                billerid : this.state.reqInsurance.billerid,
                productid : this.state.reqInsurance.productid,
                userid : this.state.reqInsurance.userid,
                partnerid : this.state.reqInsurance.partnerid,
                publickey : this.state.reqInsurance.publickey,
                customerid : res.data.customerid,
                customername : res.data.customername,
                totalamount : res.data.totalamount,
                additionaldata : res.data.additionaldata,
                addDatas : addDatas,
                mobileno : this.state.reqInsurance.mobileno,
                email : this.state.reqInsurance.email,
                extendinfo : res.data.extendinfo,
                trackingref : res.data.trackingref,
                username : this.state.terminalid,
                flag : "",
                img_detail : res.data.imgpth,
                namaproduct : res.data.title,
                n_product_detail : res.data.title,
                link : this.state.link,
              });   
            }else{
              swal("Oops..", res.data.rcdesc , "warning");
            }
          }, (err) => {
            swal("Error", err.message , "error");
          })
          .catch((error) => {
            // console.log(error.message);
            swal("Error", error.message , "error");
          })
        }else{
          if(!this.state.reqInsurance.productid){
            swal("Oops..", "Pilih salah satu product." , "warning");
          }else if(!this.state.reqInsurance.customerid){
            swal("Oops..", "No Pelanggan harus diisi." , "warning");
          }
        }
      }
    }
  
    render(){
      return(
        <>  
        <div className="navbarmenu">
          <Link to={this.state.windowURL} className="menu-barsmenu">
            <FaIcons.FaArrowLeft /> 
          </Link>
          <h2> {this.state.nama} </h2>
        </div>  
        {/* <div className="jumbotron"></div> */}
        <div className="container-fluid main-menu">
          <div className="container2">
            <div className="h4">Pilih Partner</div>
          </div>
          <Dropdown
            placeholder='Select Partner'
            fluid
            search
            selection
            options={this.state.planets}
            onChange={this.handleChange}
          />
          <div className="container2">
            <div className="h4">No. Pelanggan</div>
            <div><input className="input" id="customerid" name="customerid" placeholder="Nomor Pelanggan" onChange={this.handleChange} /></div>
          </div>
          {
            this.state.fl_Inq === "1" ? (
              <div className="container2">
                <div className="h4">Nominal</div>
                <div><input className="input" id="nominal" name="nominal" placeholder="Nominal" onChange={this.handleChange} /></div>
              </div>
            ):(
              null
            ) 
          }
        </div>
        <div className="container-fluid button1" > 
          <hr/>
          <b.Button variant="success" name="btnproses" id="btnproses" onClick={this.postdatatoAPI} block>Continue</b.Button>
        </div>
        </>
      );
    };
  }
  
  export default Insurance;