import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import './components/mainMenu.css';
import axios from 'axios';
import swal from 'sweetalert';
import { Dropdown } from 'semantic-ui-react';
import { header,password,rand1,rand2,rand3 } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';

const { REACT_APP_URL_API } = process.env;

class TV extends Component{

    constructor(props){
      super(props);
      this._isMounted = false;
      this.state = {
        link : props.link,
        nama : props.nama,
        terminalid : props.terminalid,
        windowURL : props.windowURL,
        reqTV : {
          HEADER : header,
          billerid : props.billerid,
          customerid : "",
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          email : props.email,
          extendinfo : "0",
          mobileno : props.mobileno,
          productid : "",
          signature : ""
        },
        reqDataTV : {
          HEADER : header,
          userid : props.userid,
          partnerid : props.partnerid,
          publickey : props.publickey,
          billerId : props.billerid,
          signature : ""
        },
        planets: [],
      };

      if(this.state.reqTV.publickey === "" && this.state.windowURL === ""){
        swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
      }
    }

    componentDidMount() {
      document.body.style.background = "white";
      let windowURLNew = './solusipayweb/'+ this.state.windowURL;
      this.setState({
        windowURL: windowURLNew,
      });
      this._isMounted = true;
      // Generate Signature md5
      var md5 = require('md5');
      let signature = md5(header+this.state.reqDataTV.userid+this.state.reqDataTV.partnerid+this.state.reqDataTV.publickey+this.state.reqDataTV.billerId+password);
      // Buat Variable baru untuk mengUpdate Request Data MF
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      let reqDataTVNew = {...this.state.reqDataTV};
      reqDataTVNew['signature'] = ReSign;
      // Hit API POST 
      let initialPlanets = [];
        axios.post(REACT_APP_URL_API + 'ListProduct',reqDataTVNew).then(response => {
          return response.data.data;
      }).then(data => {
        for (let i = 0; i < data.length; i++) {
          initialPlanets[i] = {key: data[i].productid, value: data[i].productid, image: require("../"+data[i].img), text: data[i].productdesc};
        }
        // console.log(initialPlanets);
        if (this._isMounted) {
          this.setState({
            planets: initialPlanets,
          });
        }
      }).catch((error) => {
        // console.log(error.message);
        swal({title: "ERROR - Server sedang maintenance.", text: "Kembali ke halaman utama.", icon: "error"}).then(function() {
          window.location = "./solusipayweb";
        });
      });
    }

    componentWillUnmount() {
      this._isMounted = false;
    }

    handleChange = (event,data) => {
      let reqTVNew = {...this.state.reqTV}; //mengcopy variable dengan isi sama
      if (data){
        // console.log(data);
        reqTVNew['productid'] = data.value;
      }else{
        // console.log(event.target.name,event.target.value);
        reqTVNew[event.target.name] = event.target.value;
      }
      this.setState({
        reqTV: reqTVNew
      })
    }
  
    postdatatoAPI = () => {
      if(this.state.reqTV.customerid && this.state.reqTV.productid)
      {
        swal({
          text: "Mohon Tunggu",
          icon: require("./icon/loading3.gif"),
          buttons: false,
          closeOnClickOutside : false
        });
        // Create Signature
        var md5 = require('md5');
        let signature = md5(header+this.state.reqTV.userid+this.state.reqTV.partnerid+this.state.reqTV.publickey+this.state.reqTV.billerid+this.state.reqTV.customerid+this.state.reqTV.mobileno +this.state.reqTV.productid+password);
        let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
        let reqTVNew = {...this.state.reqTV};
        reqTVNew['signature'] = ReSign;
        console.log("REQ TV: ",reqTVNew);
        // hit API
        axios.post(REACT_APP_URL_API+'MobileInq',reqTVNew).then((res) => {
          console.log("RES TV: ",res.data);
          if(res.data.rc === "00"){
            swal.close();
            const addDatas = JSON.parse(res.data.additionaldata);
            this.props.history.push('/detilpembayaran',{
              billerid : this.state.reqTV.billerid,
              productid : this.state.reqTV.productid,
              userid : this.state.reqTV.userid,
              partnerid : this.state.reqTV.partnerid,
              publickey : this.state.reqTV.publickey,
              customerid : res.data.customerid,
              customername : res.data.customername,
              totalamount : res.data.totalamount,
              additionaldata : res.data.additionaldata,
              addDatas : addDatas,
              mobileno : this.state.reqTV.mobileno,
              email : this.state.reqTV.email,
              extendinfo : res.data.extendinfo,
              trackingref : res.data.trackingref,
              username : this.state.terminalid,
              flag : "",
              img_detail : res.data.imgpth,
              namaproduct : res.data.title,
              n_product_detail : res.data.title,
              link : this.state.link,
            }); 
          }else{
            swal("Oops..", res.data.rcdesc , "warning");
          }
        }, (err) => {
          swal("Error", err.message , "error");
        })
        .catch((error) => {
          // console.log(error.message);
          swal("Error", error.message , "error");
        })
      }else{
        if(!this.state.reqTV.productid){
          swal("Oops..", "Pilih salah satu product." , "warning");
        }else if(!this.state.reqTV.customerid){
          swal("Oops..", "No Pelanggan harus diisi." , "warning");
        }
      }
    }
  
    render(){
      return(
        <>  
        <div className="navbarmenu">
          <Link to={this.state.windowURL} className="menu-barsmenu">
            <FaIcons.FaArrowLeft /> 
          </Link>
          <h2> {this.state.nama} </h2>
        </div>  
        {/* <div className="jumbotron"></div> */}
        <div className="container-fluid main-menu">
          <div className="container2">
            <div className="h4">Pilih Partner</div>
          </div>
          <Dropdown
            placeholder='Select Partner'
            fluid
            search
            selection
            options={this.state.planets}
            onChange={this.handleChange}
          />
          <div className="container2">
            <div className="h4">No. Pelanggan</div>
            <div><input className="input" type="number" id="customerid" min="0" name="customerid" placeholder="Nomor Pelanggan" onChange={this.handleChange} /></div>
          </div>
        </div>
        <div className="container-fluid button1" > 
          <hr/>
          <b.Button variant="success" name="btnproses" id="btnproses" onClick={this.postdatatoAPI} block>Continue</b.Button>
        </div>
        </>
      );
    };
  }
  
  export default TV;