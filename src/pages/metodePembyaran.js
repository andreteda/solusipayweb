import React, {Component} from 'react';
import * as b from 'react-bootstrap';
import { header,password,rand1,rand2,rand3,respassword } from './data/mainData';
import * as FaIcons from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './components/mainMenu.css';
import NumberFormat from 'react-number-format';
import axios from 'axios';
import swal from 'sweetalert';
import dateFormat from 'dateformat';

const { REACT_APP_URL_API } = process.env;

class MetodePembayaran extends Component{
  constructor(props){
    super(props);
    this._isMounted = false;
    this.state = {
      nama : props.nama,
      publickey : props.publickey,
      img_detail : props.history.location.state.img_detail,
      namaproduct : props.history.location.state.namaproduct,
      n_product_detail : props.history.location.state.n_product_detail,
      link : props.link,
      listMetodeByr : props.history.location.state.listMetodeBayar,
      totalamount : props.history.location.state.totalamount,
      customerid : props.history.location.state.customerid,
      customername : props.history.location.state.customername,
      billerid : props.history.location.state.billerid,
      windowURL : props.windowURL,
      reqMetodePay : {
        HEADER: header,
        bookid: props.history.location.state.bookingid,
        paymetode: "",
        orderid: props.history.location.state.orderid,
        signature: "",
        paymetodedesc: "",
        userid: props.history.location.state.userid,
        partnerid: props.history.location.state.partnerid,
        publickey: props.history.location.state.publickey
      },
    }
  }

  componentDidMount(){
    this._isMounted = true;
    let windowURLNew = './solusipayweb/'+ this.state.windowURL;
    this.setState({
      windowURL: windowURLNew,
    });
    // console.log(this.state.reqMetodePay);
    // console.log(this.state.listMetodeByr);

    if(this.state.publickey === "" && this.state.windowURL === ""){
      swal("Info", "Your Session is ended. Please back to Home Apps." , "info");
    }
  }

  postdatatoAPI = (metodeBayar,metodeBayarDesc, imgpath) => {
    swal({
      text: "Mohon Tunggu",
      icon: require("./icon/loading3.gif"),
      buttons: false,
      closeOnClickOutside : false
    });
    if(metodeBayar === "11"){
      swal("Oops..", "WalletPay Page" , "warning");
    }else{
      // Generate Signature md5
      var md5 = require('md5');
      let signature = md5(header+this.state.reqMetodePay.userid+this.state.reqMetodePay.partnerid+this.state.reqMetodePay.publickey+this.state.reqMetodePay.bookid+this.state.reqMetodePay.orderid+metodeBayar+password);
      let ReSign = rand3+signature.substring(0,8)+rand1+signature.substring(8,16)+rand2+signature.substring(16,32);
      let ressignature = md5(this.state.reqMetodePay.userid+this.state.reqMetodePay.partnerid+this.state.reqMetodePay.publickey+respassword);
      let reqMetodePayNew = {...this.state.reqMetodePay};
      reqMetodePayNew['signature'] = ReSign;
      reqMetodePayNew['paymetode'] = metodeBayar;
      reqMetodePayNew['paymetodedesc'] = metodeBayarDesc;

      console.log("REQ METODE PAY: ", reqMetodePayNew);

      axios.post(REACT_APP_URL_API + 'metodepay',reqMetodePayNew).then(response => {
        console.log("RES METODE PAY: ",response.data);
        return response.data;
      }).then(data => {
        if(data.rc === "00"){
          swal.close();
          if (metodeBayar === 14 || metodeBayar === 15) {
            swal("Oops..", "PAYMENT METHOD 14 & 15" , "warning");
          } else {
            // console.log("imgpath: ", this.state.img_detail);
            if (ressignature === data.signature) {
              // console.log("berhasil ke hasil bayar");
              let date = new Date();
              date.setHours( date.getHours() + 2 );
              // console.log(dateFormat(date, 'dd mmmm yyyy , HH:MM'));

              this.props.history.push('/hasilpembayaran',{
                nova: data.nova,
                paymetode: data.paymetode,
                billerid: this.state.billerid,
                customername: this.state.customername,
                title: "",
                amount: this.state.totalamount,
                customerid: this.state.customerid,
                imgpth: this.state.img_detail,
                link: this.state.link,
                totalamount : this.state.totalamount,
                namaproduct : this.state.namaproduct,
                paymentID : metodeBayar,
                paymethdesc : metodeBayarDesc,
                imgpath : imgpath,
                dateNew : dateFormat(date, 'dd mmmm yyyy , HH:MM'),
              });
            };
            // if (Ressign == res.signature) {
            //   swal("Oops..", "Error Page" , "warning");
            // }
          }
        }else{
          swal("Oops..", data.rcdesc , "warning");
        }
      }).catch((error) => {
        // console.log(error.message);
        swal({title: error.message, text: "Kembali ke halaman sebelumnya.", icon: "error"}).then(function() {
          // window.location = "./solusipayweb";
        });
      });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }
  
  render(){
    return(
      <>  
      <div className="navbarmenu">
        <Link to={this.state.windowURL} className="menu-barsmenu">
          <FaIcons.FaArrowLeft /> 
        </Link>
        <h2> {this.state.nama} </h2>
      </div>  
      <br></br>
      <div className="container-fluid" > 
        <b.Card className="cardMetode">
          <div className="container2">
            <div>
              <img src={require("../"+ this.state.img_detail)} alt="" className="iconDetails2" />
            </div>
            <div className="h5">{this.state.namaproduct}</div>
            <div className="h5">{this.state.customerid} a/n {this.state.customername}</div>
            <div className="h5">Total: <NumberFormat value={this.state.totalamount} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></div>
          </div>
        </b.Card>
        <br></br>
        <div>Choose Payment Method </div>
        <b.ListGroup>
          {this.state.listMetodeByr.map((item, index) => {
              return (
                <b.ListGroup.Item className="lgItem" action key={item.item_1} onClick={this.postdatatoAPI.bind(this, item.item_1, item.item_2, item.item_3)} name="nominal" id="nominal" value={item.nominal_detail}>
                  <div className="container2">
                    <div>
                      <img src={require("../"+item.item_3)} alt="" className="iconDetailsMetode" />
                    </div>
                    <div>
                      <div className="h4">{item.item_2}</div>
                    </div>
                  </div>
                </b.ListGroup.Item>
              )
          })}
        </b.ListGroup>
        </div>
      </>
    )
  }
}

export default MetodePembayaran;