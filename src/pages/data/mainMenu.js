export const MainMenuData =[
  {
    title: 'PLN',
    path: '/pln',
    image: require('../icon/pln.png'),
    cName: 'nav-text',
    group: 'Tagihan'
  },
  {
    title: 'BPJS',
    path: '/bpjs',
    image: require('../icon/bpjs.png'),
    cName: 'nav-text',
    group: 'Asuransi Pribadi'
  },
  {
    title: 'Multifinance',
    path: '/multifinance',
    image: require('../icon/multifinance.png'),
    cName: 'nav-text',
    group: 'Tagihan'
  },
  {
    title: 'Air PDAM',
    path: '/pdam',
    image: require('../icon/pdam.png'),
    cName: 'nav-text',
    group: 'Tagihan'
  },
  {
    title: 'Pascabayar',
    path: '/postpaid',
    image: require('../icon/postpaid.png'),
    cName: 'nav-text',
    group: 'Isi Pulsa'
  },
  {
    title: 'Insurance',
    path: '/insurance',
    image: require('../icon/insurance.png'),
    cName: 'nav-text',
    group: 'Asuransi Pribadi'
  },
  {
    title: 'TV',
    path: '/tv',
    image: require('../icon/tv.png'),
    cName: 'nav-text',
    group: 'Tagihan'
  },
  {
    title: 'Telkom',
    path: '/telkom',
    image: require('../icon/telkom.png'),
    cName: 'nav-text',
    group: 'Tagihan'
  },
  {
    title: 'Pulsa',
    path: '/pulsa',
    image: require('../icon/pulsa.png'),
    cName: 'nav-text',
    group: 'Isi Pulsa'
  },
  {
    title: 'Voucher Game',
    path: '/game',
    image: require('../icon/game.png'),
    cName: 'nav-text',
    group: 'Hiburan'
  },
  {
    title: 'Voucher',
    path: '/voucher',
    image: require('../icon/voucher.png'),
    cName: 'nav-text',
    group: 'Hiburan'
  },
  {
    title: 'PGN',
    path: '/pgn',
    image: require('../icon/pgn.png'),
    cName: 'nav-text',
    group: 'Tagihan'
  },
  // {
  //   title: 'E-Samsat',
  //   path: '/esamsat',
  //   image: require('../icon/esamsat.png'),
  //   cName: 'nav-text'
  // }
];