import React, {} from 'react';
import * as b from 'react-bootstrap';

function BCA() {
  return (
    <>
    <b.Accordion defaultActiveKey="0">
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="0">
            ATM 
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="0">
            <b.Card.Body>
              1. Masukkan kartu ATM BCA & PIN. <br></br>
              2. Pilih menu "Transaksi Lainnya". <br></br>
              3. Pilih menu "Transfer". <br></br>
              4. Pilih menu "Ke Rekening BCA Virtual Account". <br></br>
              5. Masukkan nomor Virtual Account. <br></br>
              6. Masukkan jumlah pembayaran.
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="1">
            m-BCA (BCA Mobile)
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="1">
            <b.Card.Body>
              1. Login dengan ID dan password Mobile BCA Anda. <br></br>
              2. Pilih menu TRANSFER > VIRTUAL ACCOUNT. <br></br>
              3. Masukkan nomor virtual account Anda. <br></br>
              4. Tekan tombol LANJUT. <br></br>
              5. Setelah muncul informasi pembayaran pastikan jumlah tagihan sesuai dengan yang tertera pada detail transaksi Anda. <br></br>
              6. Masukkan PIN Mobile BCA Anda lalu tekan OK. <br></br>
              7. Setelah pembayaran sukses simpan struk pembayaran Anda sebagai bukti yang sah.
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
        <b.Card>
          <b.Accordion.Toggle as={b.Card.Header} eventKey="2">
            Internet (Klik BCA)
          </b.Accordion.Toggle>
          <b.Accordion.Collapse eventKey="2">
            <b.Card.Body>
              1. Login dengan ID dan password Anda ke Internet Banking BCA. <br></br>
              2. Pilih menu TRANSFER > VIRTUAL ACCOUNT. <br></br>
              3. Masukkan nomor virtual account Anda. <br></br>
              4. Tekan tombol LANJUT. <br></br>
              5. Setelah muncul informasi pembayaran pastikan jumlah tagihan sesuai dengan yang tertera pada detail transaksi Anda. <br></br>
              6. Masukkan PIN Mobile BCA Anda lalu tekan OK. <br></br>
              7. Setelah pembayaran sukses simpan struk pembayaran Anda sebagai bukti yang sah.
            </b.Card.Body>
          </b.Accordion.Collapse>
        </b.Card>
      </b.Accordion>
    </>
  )
}

export default BCA;
